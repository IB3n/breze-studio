import { Component } from '@angular/core';

@Component({
  selector: 'bz-p-app',
  templateUrl: './p-app.component.html',
  styleUrls: ['./p-app.component.scss'],
})
export class PAppComponent {}
