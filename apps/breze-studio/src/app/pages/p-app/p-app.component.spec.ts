import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PAppComponent } from './p-app.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/compiler/src/core';
import { RouterTestingModule } from '@angular/router/testing';

describe('PAppComponent', () => {
  let component: PAppComponent;
  let fixture: ComponentFixture<PAppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PAppComponent],
      imports: [RouterTestingModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PAppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
