import { Component } from '@angular/core';

@Component({
  selector: 'bz-p-homepage',
  templateUrl: './p-homepage.component.html',
  styleUrls: ['./p-homepage.component.scss'],
})
export class PHomepageComponent {
  homepageLinks = [
    {
      title: 'Create custom progressions',
      description:
        'build Guitar & Ukulele progression with scale to enpower your creativity',
      link: {
        label: 'Try now !',
        target: ['/', 'app', 'dashboard'],
      },
    },
    {
      title: 'Explore chords',
      description:
        'You got a non standard tuning ? We got your back with our chord generator',
      link: {
        label: 'Try now !',
        target: ['/', 'app', 'chords'],
      },
    },
  ];
}
