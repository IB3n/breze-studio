import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PHomepageComponent } from './pages/p-homepage/p-homepage.component';

const routes: Routes = [
  {
    path: '',
    component: PHomepageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeRoutingModule {}
