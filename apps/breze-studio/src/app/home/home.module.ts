import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { NavigationModule } from '../navigation/navigation.module';
import { HomeRoutingModule } from './home-routing.module';
import { PHomepageComponent } from './pages/p-homepage/p-homepage.component';

@NgModule({
  declarations: [PHomepageComponent],
  imports: [HomeRoutingModule, CommonModule, NavigationModule],
})
export class HomeModule {}
