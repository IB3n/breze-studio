import { Component } from '@angular/core';
import { NavigationItem } from '../../../interface/navigation';

@Component({
  selector: 'bz-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss'],
})
export class NavigationComponent {
  navigationItems: Array<NavigationItem> = [
    {
      label: 'Live View',
      routerLink: ['/', 'app', 'dashboard'],
    },
    {
      label: 'Chords',
      routerLink: ['/', 'app', 'chords'],
    },
  ];
}
