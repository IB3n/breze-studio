import { Component } from '@angular/core';

@Component({
  selector: 'bz-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent {}
