import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { NavbarComponent } from './components/navbar/navbar.component';
import { NavigationComponent } from './components/navigation/navigation.component';

const ioDeclarations = [NavbarComponent, NavigationComponent];

@NgModule({
  declarations: ioDeclarations,
  exports: ioDeclarations,
  imports: [CommonModule, RouterModule],
})
export class NavigationModule {}
