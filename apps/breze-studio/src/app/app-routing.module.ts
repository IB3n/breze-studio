import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PAppComponent } from './pages/p-app/p-app.component';

const routes: Routes = [
  {
    path: 'app',
    component: PAppComponent,
    children: [
      {
        path: 'dashboard',
        loadChildren: () =>
          import('./dashboard/dashboard.module').then((m) => m.DashboardModule),
      },
      {
        path: 'chords',
        loadChildren: () =>
          import('./chords/chords.module').then((m) => m.ChordsModule),
      },
    ],
  },
  {
    path: '',
    loadChildren: () => import('./home/home.module').then((m) => m.HomeModule),
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
