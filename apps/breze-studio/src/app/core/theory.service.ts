import { Injectable } from "@angular/core";
import { BrezeMusicTheory, ChordService, NoteService, ProgressionService, ScaleService } from "@breze-studio/music-theory";

@Injectable({
  providedIn: "root"
})
export class TheoryService {

  noteService: NoteService;
  chordService: ChordService;
  progressionService: ProgressionService;
  scaleService: ScaleService;

  constructor() {
    Object.assign(this, new BrezeMusicTheory());
  }
}