import { List } from 'immutable';

export interface Instrument {
  strings: Array<List<boolean>>;
  buildString: (note: string, length: number) => Array<string>;
  toString: () => string;
}
