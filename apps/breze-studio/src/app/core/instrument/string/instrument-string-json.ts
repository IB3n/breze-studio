import { NoteWithHeight } from '../../../interface/note-with-height';

export interface InstrumentStringJSON {
  note: NoteWithHeight;
  length: number;
  notes: Array<NoteWithHeight>;
}
