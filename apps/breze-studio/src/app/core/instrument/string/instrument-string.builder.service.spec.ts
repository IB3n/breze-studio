import { TheoryService } from '../../theory.service';
import { InstrumentStringBuilderService } from './instrument-string.builder.service';

describe('InstrumentStringBuilderService', () => {
  let stringBuilder: InstrumentStringBuilderService;

  beforeEach(() => {
    stringBuilder = new InstrumentStringBuilderService(new TheoryService());
  });

  describe('building', () => {
    it('note sets note to inner json', () => {
      stringBuilder.note({
        note: 'A',
        height: 0,
      });
      expect(stringBuilder.instrumentStringJSON.note).toEqual({
        note: 'A',
        height: 0,
      });
    });
    it('length sets length to inner json', () => {
      stringBuilder.length(24);
      expect(stringBuilder.instrumentStringJSON.length).toBe(24);
    });

    it('when calling build, it create a notes array into innerJson', () => {
      stringBuilder.note({
        note: 'G',
        height: 4,
      });
      stringBuilder.length(2);
      stringBuilder.build();
      expect(stringBuilder.instrumentStringJSON.notes).toEqual([
        { note: 'G', height: 4 },
        { note: 'G#', height: 4 },
      ]);
    });
  });
});
