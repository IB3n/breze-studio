import { NoteWithHeight } from '../../../interface/note-with-height';
import { InstrumentStringJSON } from './instrument-string-json';

export class InstrumentString {
  readonly note: NoteWithHeight;
  readonly length: number;
  readonly notes: Array<NoteWithHeight>;

  constructor(instrumentStringJson: InstrumentStringJSON) {
    this.note = instrumentStringJson.note;
    this.length = instrumentStringJson.length;
    this.notes = instrumentStringJson.notes;
  }

  toJSON(): InstrumentStringJSON {
    return {
      note: this.note,
      length: this.length,
      notes: this.notes,
    };
  }
}
