import { Injectable } from '@angular/core';

import { InstrumentString } from './instrument-string';

@Injectable({
  providedIn: 'root',
})
export class InstrumentStringUtilsService {
  /**
   * getPosition returns the position of a note (simple anglican name, no height) on a string,
   * it will return -1 if not found, the position index otherwise.
   */
  getPosition(note: string, strings: InstrumentString) {
    let position = -1;
    const nbNotes = strings.notes.length;
    for (let i = 0; i < nbNotes; i++) {
      if (note === strings.notes[i].note) {
        position = i;
        break;
      }
    }
    return position;
  }
}
