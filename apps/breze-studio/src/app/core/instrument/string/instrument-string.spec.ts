import { InstrumentString } from './instrument-string';
import { InstrumentStringJSON } from './instrument-string-json';

describe('InstrumentString', () => {
  describe('construction', () => {
    it('match to length if present in parameter', () => {
      const strJSON = {
        length: 4,
      } as InstrumentStringJSON;
      const str = new InstrumentString(strJSON);
      expect(str.length).toBe(strJSON.length);
    });

    it('match to note if present in parameter', () => {
      const strJSON = {
        note: { note: 'A', height: 4 },
      } as InstrumentStringJSON;
      const str = new InstrumentString(strJSON);
      expect(str.note).toBe(strJSON.note);
    });

    it('match to notes if present in parameter', () => {
      const strJSON = {
        note: {
          note: 'A',
          height: 4,
        },
        length: 5,
        notes: [
          {
            note: 'A',
            height: 4,
          },
        ],
      } as InstrumentStringJSON;
      const str = new InstrumentString(strJSON);
      expect(str.notes).toBe(strJSON.notes);
    });
  });

  describe('toJSON', () => {
    it('constrution then toJSON is an identity', () => {
      const strJSON = {
        note: {
          note: 'A',
          height: 4,
        },
        length: 5,
        notes: [],
      } as InstrumentStringJSON;
      const str = new InstrumentString(strJSON);
      expect(str.toJSON()).toEqual(strJSON);
    });
  });
});
