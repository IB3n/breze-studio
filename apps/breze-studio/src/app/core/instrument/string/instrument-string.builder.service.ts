import { Injectable } from '@angular/core';
import { NoteWithHeight } from '../../../interface/note-with-height';
import { TheoryService } from '../../theory.service';

import { InstrumentString } from './instrument-string';
import { InstrumentStringJSON } from './instrument-string-json';

@Injectable({
  providedIn: 'root',
})
export class InstrumentStringBuilderService {
  instrumentStringJSON: InstrumentStringJSON;

  constructor(private theoryService: TheoryService) {
    this.instrumentStringJSON = {} as InstrumentStringJSON;
  }

  note(note: NoteWithHeight) {
    this.instrumentStringJSON.note = note;
    return this;
  }

  length(length: number) {
    this.instrumentStringJSON.length = length;
    return this;
  }

  build() {
    this.instrumentStringJSON.notes = ' '
      .repeat(this.instrumentStringJSON.length)
      .split('')
      .map((_, idx) =>
        this.theoryService.noteService.getStepsAfterWithHeight(
          this.instrumentStringJSON.note,
          idx
        )
      );
    return new InstrumentString(this.instrumentStringJSON);
  }
}
