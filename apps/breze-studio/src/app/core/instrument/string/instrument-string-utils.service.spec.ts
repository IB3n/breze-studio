import { NoteWithHeight } from '../../../interface/note-with-height';
import { TheoryService } from '../../theory.service';
import { InstrumentString } from './instrument-string';
import { InstrumentStringUtilsService } from './instrument-string-utils.service';
import { InstrumentStringBuilderService } from './instrument-string.builder.service';

describe('InstrumentStringUtilsService', () => {
  let service: InstrumentStringUtilsService;
  let E2: NoteWithHeight;
  let F2: NoteWithHeight;
  let string: InstrumentString;

  beforeEach(() => {
    const builder = new InstrumentStringBuilderService(new TheoryService());
    string = builder
      .note({
        note: 'E',
        height: 2,
      })
      .length(5)
      .build();
    E2 = {
      note: 'E',
      height: 2,
    };
    F2 = {
      note: 'F',
      height: 2,
    };

    service = new InstrumentStringUtilsService();
  });

  describe('getPosition', () => {
    it('returns a number', () => {
      expect(typeof service.getPosition(E2.note, string)).toBe('number');
    });

    it('returns -1 if not found', () => {
      expect(service.getPosition('D', string)).toBe(-1);
    });

    it('returns index if found', () => {
      expect(service.getPosition('E', string)).toBe(0);
      expect(service.getPosition(F2.note, string)).toBe(1);
    });
  });
});
