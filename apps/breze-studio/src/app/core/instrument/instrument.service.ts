import { Injectable } from '@angular/core';
import { List } from 'immutable';

import { InstrumentFactory } from './instrument.factory';

@Injectable({
  providedIn: 'root',
})
export class InstrumentService {
  instruments = List([]);

  constructor(private instrumentFactory: InstrumentFactory) {
    this.instruments = this.instruments.push(
      this.instrumentFactory.createInstrument(
        'Standard Guitar',
        [
          { note: 'E', height: 4 },
          { note: 'B', height: 3 },
          { note: 'G', height: 3 },
          { note: 'D', height: 3 },
          { note: 'A', height: 2 },
          { note: 'E', height: 2 },
        ].reverse(),
        24
      ),
      this.instrumentFactory.createInstrument(
        'Standard Ukulele',
        [
          { note: 'A', height: 4 },
          { note: 'E', height: 4 },
          { note: 'C', height: 4 },
          { note: 'G', height: 4 },
        ].reverse(),
        16
      )
    );
  }
}
