import { Synth } from 'tone';

export const instrumentSynth = Synth.bind({
  oscillator: {
    type: 'sine8',
  },
  envelope: {
    attack: 2,
    decay: 1,
    sustain: 0.4,
    release: 4,
  },
});
