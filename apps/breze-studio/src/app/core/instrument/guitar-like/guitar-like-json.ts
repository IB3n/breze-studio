import { InstrumentStringJSON } from '../string/instrument-string-json';

export interface GuitarLikeJSON {
  label: string;
  strings: Array<InstrumentStringJSON>;
}
