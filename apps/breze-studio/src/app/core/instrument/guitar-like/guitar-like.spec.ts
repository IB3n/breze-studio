import { InstrumentString } from '../string/instrument-string';
import { InstrumentStringJSON } from '../string/instrument-string-json';
import { GuitarLike } from './guitar-like';
import { GuitarLikeJSON } from './guitar-like-json';

describe('Guitar', () => {
  describe('construction', () => {
    it('match to label if provided', () => {
      const guitarLikeJSON = {
        label: 'default',
        strings: [],
      } as GuitarLikeJSON;
      const guitar = new GuitarLike(guitarLikeJSON);
      expect(guitar.label).toBe(guitarLikeJSON.label);
    });

    it('match to strings if present in parameter', () => {
      const stringA: InstrumentStringJSON = {
        note: { note: 'A', height: 4 },
        length: 20,
        notes: [],
      };
      const guitarLikeJSON = {
        strings: [stringA],
      } as GuitarLikeJSON;
      const guitar = new GuitarLike(guitarLikeJSON);
      expect(guitar.strings.get(0)).toEqual(new InstrumentString(stringA));
    });
  });

  describe('toJSON', () => {
    it('constrution then toJSON is an identity', () => {
      const stringA: InstrumentStringJSON = {
        note: {
          note: 'A',
          height: 7,
        },
        length: 20,
        notes: [],
      };
      const guitarLikeJSON = {
        strings: [stringA],
      } as GuitarLikeJSON;
      const str = new GuitarLike(guitarLikeJSON);
      expect(str.toJSON()).toEqual(guitarLikeJSON);
    });
  });
});
