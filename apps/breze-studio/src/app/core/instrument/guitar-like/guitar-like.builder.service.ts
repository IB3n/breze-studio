import { Injectable } from '@angular/core';

import { InstrumentStringJSON } from '../string/instrument-string-json';
import { GuitarLike } from './guitar-like';
import { GuitarLikeJSON } from './guitar-like-json';

@Injectable({
  providedIn: 'root',
})
export class GuitarLikeBuilderService {
  guitarLikeJSON: GuitarLikeJSON;

  constructor() {
    this.guitarLikeJSON = {} as GuitarLikeJSON;
  }

  label(label: string) {
    this.guitarLikeJSON.label = label;
    return this;
  }

  strings(strings: Array<InstrumentStringJSON>) {
    this.guitarLikeJSON.strings = strings;
    return this;
  }

  build() {
    return new GuitarLike(this.guitarLikeJSON);
  }
}
