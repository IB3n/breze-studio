import { List } from 'immutable';

import { InstrumentString } from '../string/instrument-string';
import { InstrumentStringJSON } from '../string/instrument-string-json';
import { GuitarLikeJSON } from './guitar-like-json';

export class GuitarLike {
  readonly label: string;
  readonly strings: List<InstrumentString>;

  constructor(guitarLikeJSON: GuitarLikeJSON) {
    this.label = guitarLikeJSON.label;
    this.strings = List(
      guitarLikeJSON.strings.map(
        (str: InstrumentStringJSON) => new InstrumentString(str)
      )
    );
  }

  /**
   * getPositionOnString returns the position of a note (simple anglican name, no height) on a string,
   * it will return -1 if not found, the position index otherwise.
   */
  getPositionOnString(note: string, strings: InstrumentString) {
    let position = -1;
    const nbNotes = strings.notes.length;
    for (let i = 0; i < nbNotes; i++) {
      if (note === strings.notes[i].note) {
        position = i;
        break;
      }
    }
    return position;
  }

  getPosition(chord: Array<string>) {
    const positions = ' '
      .repeat(6)
      .split('')
      .map(() => null);
    this.strings.forEach((string: InstrumentString, key: number) => {
      // String number ${key}
      chord.forEach((chordNote: string) => {
        // Set the position on the lowest possible note
        // TODO Correction needed, no 7th or 9th possible...
        const chordNotePosition = this.getPositionOnString(
          chordNote,
          string
        );
        if (positions[key] === null && chordNotePosition !== -1) {
          positions[key] = chordNotePosition;
        } else if (positions[key] > chordNotePosition) {
          positions[key] = chordNotePosition;
        }
      });
    });
    return positions;
  }

  getScalePositions(
    scale: Array<string>,
    instrument: GuitarLike,
    maxFret = 12
  ) {
    const positions = ' '
      .repeat(6)
      .split('')
      .map(() => null);
    instrument.strings.forEach((string: InstrumentString, key: number) => {
      // String number ${key}
      positions[key] = [];
      scale.forEach((chordNote: string) => {
        // Set the position on the lowest possible note
        const chordNotePosition = this.getPositionOnString(
          chordNote,
          string
        );
        if (
          (positions[key] !== null || chordNotePosition !== -1) &&
          chordNotePosition < maxFret
        ) {
          positions[key].push(chordNotePosition);
        }
      });
    });
    return positions;
  }
  
  toJSON(): GuitarLikeJSON {
    return {
      label: this.label,
      strings: Array.from(this.strings).map((string: InstrumentString) =>
        string.toJSON()
      ),
    };
  }
}
