import { Injectable } from '@angular/core';
import { NoteWithHeight } from '../../interface/note-with-height';
import { TheoryService } from '../theory.service';

import { GuitarLike } from './guitar-like/guitar-like';
import { GuitarLikeBuilderService } from './guitar-like/guitar-like.builder.service';
import { InstrumentStringBuilderService } from './string/instrument-string.builder.service';

@Injectable({
  providedIn: 'root',
})
export class InstrumentFactory {
  guitar: GuitarLike;

  constructor(
    private guitarBuilder: GuitarLikeBuilderService,
    private stringBuilder: InstrumentStringBuilderService
  ) {}

  createInstrument(name: string, notes: Array<NoteWithHeight>, nbFret: number) {
    const instrument = this.guitarBuilder
      .label(name)
      .strings(
        notes.map((note: NoteWithHeight) =>
          this.stringBuilder.note(note).length(nbFret).build().toJSON()
        )
      )
      .build();
    return instrument;
  }
}
