import { GuitarLike } from './guitar-like/guitar-like';

export interface InstrumentState {
  instrument: GuitarLike;
  positions: Array<number>;
}
