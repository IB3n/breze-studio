import { AbstractControl } from '@angular/forms';

import { AnglicanNotesDict } from '../theory/note/anglican_notes';

export const NoteValidator = (control: AbstractControl) => {
  const legitNote = AnglicanNotesDict.has(control.value);
  return legitNote ? null : { value: control.value };
};
