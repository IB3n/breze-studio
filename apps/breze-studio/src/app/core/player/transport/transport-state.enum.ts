export enum TransportState {
  STARTED = 4,
  PAUSED = 3,
  STOPPED = 2,
  LOOP = 1,
}
