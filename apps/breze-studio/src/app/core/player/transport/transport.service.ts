import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Transport } from 'tone';

import { TransportState } from './transport-state.enum';

@Injectable({
  providedIn: 'root',
})
export class TransportService {
  _transportState$ = new BehaviorSubject(null);

  get transportState() {
    return this._transportState$.getValue();
  }

  set transportState(state) {
    this._transportState$.next(state);
  }

  constructor() {
    Transport.on('start', () => {
      this.transportState = TransportState.STARTED;
      console.log('[TRANSPORT] start');
    });
    Transport.on('stop', () => {
      this.transportState = TransportState.STOPPED;
      console.log('[TRANSPORT] stop');
    });
    Transport.on('pause', () => {
      this.transportState = TransportState.PAUSED;
      console.log('[TRANSPORT] pause');
    });
    Transport.on('loop', () => {
      this.transportState = TransportState.LOOP;
      console.log('[TRANSPORT] loop');
    });
  }

  start() {
    Transport.start();
  }

  scheduleOnce(cb: () => unknown, time: string) {
    if (this.transportState === TransportState.STOPPED) {
      this.start();
    }
    Transport.scheduleOnce(cb, time);
  }
}
