import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { SampleService } from './sample/sample.service';
import { TransportService } from './transport/transport.service';

@NgModule({
  declarations: [],
  imports: [CommonModule],
  providers: [SampleService, TransportService],
})
export class PlayerModule {
  constructor(
    private sampler: SampleService,
    private transportService: TransportService
  ) {
    this.transportService.start();
  }
}
