import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Meter, Sampler } from 'tone';

@Injectable({
  providedIn: 'root',
})
export class SampleService {
  loadedSample$ = new BehaviorSubject(null);
  sampler: Sampler;

  loadSample() {
    if (this.sampler) {
      return;
    }
    this.sampler = new Sampler(
      {
        A1: 'assets/guitar/A1.mp3',
        A2: 'assets/guitar/A2.mp3',
        A3: 'assets/guitar/A3.mp3',
        A4: 'assets/guitar/A4.mp3',
        A6: 'assets/guitar/A6.mp3',
        'A#1': 'assets/guitar/As1.mp3',
        'A#2': 'assets/guitar/As2.mp3',
        'A#3': 'assets/guitar/As3.mp3',
        B1: 'assets/guitar/B1.mp3',
        B2: 'assets/guitar/B2.mp3',
        B3: 'assets/guitar/B3.mp3',
        B5: 'assets/guitar/B5.mp3',
        B6: 'assets/guitar/B6.mp3',
        C2: 'assets/guitar/C2.mp3',
        C3: 'assets/guitar/C3.mp3',
        C4: 'assets/guitar/C4.mp3',
        C5: 'assets/guitar/C5.mp3',
        'C#2': 'assets/guitar/Cs2.mp3',
        'C#3': 'assets/guitar/Cs3.mp3',
        'C#4': 'assets/guitar/Cs4.mp3',
        D1: 'assets/guitar/D1.mp3',
        D2: 'assets/guitar/D2.mp3',
        D3: 'assets/guitar/D3.mp3',
        D4: 'assets/guitar/D4.mp3',
        D6: 'assets/guitar/D6.mp3',
        D7: 'assets/guitar/D7.mp3',
        'D#1': 'assets/guitar/Ds1.mp3',
        'D#2': 'assets/guitar/Ds2.mp3',
        'D#3': 'assets/guitar/Ds3.mp3',
        E1: 'assets/guitar/E1.mp3',
        E2: 'assets/guitar/E2.mp3',
        E3: 'assets/guitar/E3.mp3',
        E5: 'assets/guitar/E5.mp3',
        F1: 'assets/guitar/F1.mp3',
        F2: 'assets/guitar/F2.mp3',
        F3: 'assets/guitar/F3.mp3',
        F4: 'assets/guitar/F4.mp3',
        F6: 'assets/guitar/F6.mp3',
        F7: 'assets/guitar/F7.mp3',
        'F#1': 'assets/guitar/Fs1.mp3',
        'F#2': 'assets/guitar/Fs2.mp3',
        'F#3': 'assets/guitar/Fs3.mp3',
        G1: 'assets/guitar/G1.mp3',
        G2: 'assets/guitar/G2.mp3',
        G3: 'assets/guitar/G3.mp3',
        G5: 'assets/guitar/G5.mp3',
        'G#1': 'assets/guitar/Gs1.mp3',
        'G#2': 'assets/guitar/Gs2.mp3',
        'G#3': 'assets/guitar/Gs3.mp3',
      },
      () => {
        this.sampler.toMaster();
        console.log(new Meter());
        console.log(this.sampler);
        this.loadedSample$.next(true);
      }
    );
  }
}
