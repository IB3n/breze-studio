import { NoteWithHeight } from './note-with-height';

export interface DiagramDotPosition {
  y: number;
  stringName: NoteWithHeight;
  position: number;
  x: number;
}
