import { ChordPosition } from './chord-position';

export interface NoteChords {
  note: string;
  chords: Array<ChordPosition>;
}
