import { GenericCardState } from '../dashboard/components/generic-card/generic-card-state';
import { ChordModifier } from './chord-modifier';

export interface SaveState {
  cardsData: Array<GenericCardState>;
  modifiers: Array<ChordModifier>;
}
