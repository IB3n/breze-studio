export interface ChordModifier {
  origin: string;
  target: string;
}
