import { List } from 'immutable';

import { DiagramMultipleDotPosition } from './diagram-multiple-dots-position';
import { NoteWithHeight } from './note-with-height';

export interface DiagramMultipleDotPositions {
  stringName: NoteWithHeight;
  y: number;
  positions: List<DiagramMultipleDotPosition>;
}
