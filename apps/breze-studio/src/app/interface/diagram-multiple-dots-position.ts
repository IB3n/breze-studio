export interface DiagramMultipleDotPosition {
  position: number;
  x: number;
}
