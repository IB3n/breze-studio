export interface NavigationItem {
  label: string;
  routerLink: Array<string>;
}
