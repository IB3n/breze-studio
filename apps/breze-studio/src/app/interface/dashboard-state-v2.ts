export interface DashboardStateV2 {
  progression: Array<string>;
  root: string;
}
