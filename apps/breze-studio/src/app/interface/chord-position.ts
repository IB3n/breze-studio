export interface ChordPosition {
  name: string;
  positions: Array<number>;
}
