export interface DiagramMargin {
  top: number;
  bottom: number;
  left: number;
  right: number;
}
