import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { PlayerModule } from '../core/player/player.module';
import { SharedModule } from '../shared/shared.module';
import { GenericCardComponent } from './components/generic-card/generic-card.component';
import { ModifierComponent } from './components/modifier/modifier.component';
import { ProgressionPickerComponent } from './components/progression-picker/progression-picker.component';
import { ProgressionComponent } from './components/progression/progression.component';
import { ScaleComponent } from './components/scale/scale.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { PDashboardComponent } from './pages/p-dashboard/p-dashboard.component';
import { ModifiersService } from './services/modifiers/modifiers.service';
import { DegreePickerComponent } from './components/degree-picker/degree-picker.component';
import { RootPickerComponent } from './components/root-picker/root-picker.component';

@NgModule({
  declarations: [
    PDashboardComponent,
    GenericCardComponent,
    ModifierComponent,
    ProgressionComponent,
    ScaleComponent,
    ProgressionPickerComponent,
    DegreePickerComponent,
    RootPickerComponent,
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    ReactiveFormsModule,
    SharedModule,
    PlayerModule,
  ],
  providers: [ModifiersService],
})
export class DashboardModule {}
