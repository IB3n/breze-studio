import { Component, Input, OnChanges } from '@angular/core';
import { ValuedMode } from '@breze-studio/music-theory';
import { List } from 'immutable';
import { GuitarLike } from '../../../core/instrument/guitar-like/guitar-like';
import { InstrumentString } from '../../../core/instrument/string/instrument-string';
import { TheoryService } from '../../../core/theory.service';
import { DiagramMargin } from '../../../interface/diagram-margin';
import { DiagramMultipleDotPositions } from '../../../interface/diagram-multiple-dots-positions';

import { ModifiersService } from '../../services/modifiers/modifiers.service';
import { InstrumentScaleState } from './instrument-scale-state';

@Component({
  selector: 'bz-scale',
  templateUrl: './scale.component.html',
  styleUrls: ['./scale.component.scss'],
})
export class ScaleComponent implements OnChanges {
  @Input()
  instrument: GuitarLike;

  @Input()
  key: string;

  @Input()
  progression: Array<string>;

  margin: DiagramMargin;

  strings: List<DiagramMultipleDotPositions>;
  height: number;
  width: number;
  innerHeight: number;
  innerWidth: number;
  verticalLines: Array<number>;
  fretDisplayed: number;
  fretSize: number;
  nbFrets: number;
  instrumentScaleState: InstrumentScaleState;

  constructor(
    private theoryService: TheoryService,
    private modifiersSerivce: ModifiersService
  ) {
    this.margin = {
      top: 30,
      left: 20,
      right: 10,
      bottom: 12,
    };
  }

  mostProbableMode: ValuedMode;
  chordProgression: Array<string>;

  ngOnChanges() {
    this.chordProgression = this.theoryService.progressionService.generateProgression(
      this.key,
      this.progression
    );
    this.chordProgression = this.modifiersSerivce.applyToChordProgression(
      this.chordProgression
    );
    this.mostProbableMode = this.theoryService.scaleService.getMostProbableMode(
      List(this.chordProgression)
    );
    this.instrumentScaleState = {
      instrument: this.instrument,
      positions: this.instrument.getScalePositions(
        this.mostProbableMode.result,
        this.instrument,
        12
      ),
    };
    this.nbFrets = 12;
    this.height = 150;
    this.width = this.nbFrets > 5 ? 300 : 150;
    this.innerHeight = this.height - this.margin.top - this.margin.bottom;
    this.innerWidth = this.width - this.margin.left - this.margin.right;
    this.fretSize = this.innerWidth / this.nbFrets;
    this.verticalLines = ' '
      .repeat(this.nbFrets)
      .split('')
      .map((_, id) => this.margin.left + (this.innerWidth * id) / this.nbFrets);
    const nbStrings = this.instrument.strings.size;
    this.strings = this.instrument.strings.map(
      (string: InstrumentString, idx: number) => {
        const y = this.margin.top + (idx * this.innerHeight) / (nbStrings - 1);
        return {
          stringName: string.note,
          y,
          positions: this.instrumentScaleState.positions[idx]
            ? List(
                this.instrumentScaleState.positions[idx].map(
                  (positionOnString: number) => {
                    const hasPosition = typeof positionOnString !== 'undefined';
                    return {
                      x: hasPosition
                        ? positionOnString === 0
                          ? this.margin.left
                          : this.margin.left +
                            positionOnString * this.fretSize -
                            this.fretSize / 2
                        : null,
                      position: positionOnString,
                    };
                  }
                )
              )
            : List([]),
        };
      }
    );
    console.log('new strings are', this.strings);
  }
}
