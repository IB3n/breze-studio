import { GuitarLike } from '../../../core/instrument/guitar-like/guitar-like';

export interface InstrumentScaleState {
  instrument: GuitarLike;
  positions: Array<Array<number>>;
}
