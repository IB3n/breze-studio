import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgressionPickerComponent } from './progression-picker.component';

describe('ProgressionPickerComponent', () => {
  let component: ProgressionPickerComponent;
  let fixture: ComponentFixture<ProgressionPickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProgressionPickerComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgressionPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
