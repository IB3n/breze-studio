import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
} from '@angular/core';
import { defaultProgressions } from '@breze-studio/music-theory';

@Component({
  selector: 'bz-progression-picker',
  templateUrl: './progression-picker.component.html',
  styleUrls: ['./progression-picker.component.scss'],
})
export class ProgressionPickerComponent implements OnChanges {
  @Input()
  selectedProgression = [];

  @Output()
  progressionChange = new EventEmitter<Array<string>>();

  _progression = [];

  ngOnChanges() {
    console.log(
      '[PROGRESSION-PICKER] Input progression changed, new input is',
      this.selectedProgression
    );
    this._progression = [...this.selectedProgression];
  }

  addDegree() {
    console.log('[PROGRESSION-PICKER] Add degree');
    this._progression.push('I');
    this.handleProgressionChange();
  }

  generateRandomProgression() {
    console.log('[PROGRESSION-PICKER] Generate random progression');
    const progressionIndex = Math.floor(
      Math.random() * defaultProgressions.length
    );
    this._progression = defaultProgressions[progressionIndex];
    this.handleProgressionChange();
  }

  changeDegree(id: number, degree: string) {
    console.log('[PROGRESSION-PICKER] change degree', id, 'by', degree);
    this._progression[id] = degree;
    this.handleProgressionChange();
  }

  handleProgressionChange() {
    console.log(
      '[PROGRESSION-PICKER] Emitting a new progression',
      this._progression
    );
    this.progressionChange.emit(this._progression);
  }
}
