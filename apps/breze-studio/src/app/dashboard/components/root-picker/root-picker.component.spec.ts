import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RootPickerComponent } from './root-picker.component';

describe('RootPickerComponent', () => {
  let component: RootPickerComponent;
  let fixture: ComponentFixture<RootPickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RootPickerComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RootPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
