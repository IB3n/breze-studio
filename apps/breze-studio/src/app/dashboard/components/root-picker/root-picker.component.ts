import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
} from '@angular/core';
import { MinimalDictArray } from '@breze-studio/music-theory';

@Component({
  selector: 'bz-root-picker',
  templateUrl: './root-picker.component.html',
  styleUrls: ['./root-picker.component.scss'],
})
export class RootPickerComponent implements OnChanges {
  notes = MinimalDictArray;

  @Input()
  selectedRoot = 'A';

  _selectedRoot: string;

  @Output()
  rootChange = new EventEmitter<string>();

  ngOnChanges() {
    this._selectedRoot = this.selectedRoot;
  }

  selectRoot(note: string) {
    console.log('[ROOT-PICKER] selecting', note);
    this._selectedRoot = note;
    this.handleRootChange();
  }

  handleRootChange() {
    console.log('[ROOT-PICKER] emitting root');
    this.rootChange.emit(this._selectedRoot);
  }
}
