import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { distinctUntilChanged, filter } from 'rxjs/operators';
import { TheoryService } from '../../../core/theory.service';
import { ChordModifier } from '../../../interface/chord-modifier';

@Component({
  selector: 'bz-modifier',
  templateUrl: './modifier.component.html',
  styleUrls: ['./modifier.component.scss'],
})
export class ModifierComponent implements OnChanges {
  @Input()
  id: string;

  @Input()
  inputModifier: ChordModifier;

  @Output()
  askForRemove = new EventEmitter();

  @Output()
  modifierUpdated = new EventEmitter<ChordModifier>();

  form = new FormGroup({
    origin: new FormControl('', [Validators.required]),
    target: new FormControl('', [Validators.required]),
  });

  constructor(private theoryService: TheoryService) {
    this.form.valueChanges
      .pipe(
        distinctUntilChanged(),
        filter((values: ChordModifier) => {
          return (
            this.theoryService.chordService.isValidChord(values.origin) &&
            this.theoryService.chordService.isValidChord(values.target)
          );
        })
      )
      .subscribe((modifier: ChordModifier) => {
        this.modifierUpdated.emit(modifier);
      });
  }

  ngOnChanges() {
    if (this.inputModifier) {
      console.log('changes');
      if (this.form.get('origin').value !== this.inputModifier.origin) {
        console.log('changes on origin');
        this.form.get('origin').patchValue(this.inputModifier.origin);
      }
      if (this.form.get('target').value !== this.inputModifier.target) {
        console.log(
          'changes on target form : ',
          this.form.get('target').value,
          'observable :',
          this.inputModifier.target
        );
        this.form.get('target').patchValue(this.inputModifier.target);
      }
    }
  }

  removeMe() {
    this.askForRemove.emit();
  }
}
