import { GuitarLike } from '../../../core/instrument/guitar-like/guitar-like';

export interface GenericCardState {
  instrument: GuitarLike;
  type: any;
  key: string;
  progression: Array<string>;
}
