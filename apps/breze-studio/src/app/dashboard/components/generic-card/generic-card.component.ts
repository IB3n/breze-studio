import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { List } from 'immutable';
import { GuitarLike } from '../../../core/instrument/guitar-like/guitar-like';
import { InstrumentService } from '../../../core/instrument/instrument.service';
import { DashboardStateV2 } from '../../../interface/dashboard-state-v2';

import { GenericCardState } from './generic-card-state';

export enum Types {
  SCALE = 2,
  PROGRESSION = 3,
}

@Component({
  selector: 'bz-generic-card',
  templateUrl: './generic-card.component.html',
  styleUrls: ['./generic-card.component.scss'],
})
export class GenericCardComponent implements OnChanges {
  @Input()
  cardId: number;

  @Input()
  cardState: GenericCardState;

  @Input()
  stateV2: DashboardStateV2;

  @Output()
  duplicate = new EventEmitter();

  @Output()
  update = new EventEmitter();

  @Output()
  askForRemoval = new EventEmitter();

  genericState: GenericCardState;
  optionShown = true;

  Types = Types;

  form = new FormGroup({
    instrument: new FormControl(null, Validators.required),
    type: new FormControl(null, Validators.required),
  });

  instruments: List<GuitarLike>;
  defaultProgressions: Array<Array<string>>;

  types = [
    // { id: Types.CHORD, label: 'Chord' },
    { id: Types.SCALE, label: 'Scale' },
    { id: Types.PROGRESSION, label: 'Progression' },
  ];

  constructor(private instrumentService: InstrumentService) {
    this.instruments = this.instrumentService.instruments;
  }

  ngOnChanges() {
    console.log(
      '[GENERIC-CARD]',
      '#' + this.cardId,
      'new state received',
      this.stateV2
    );
    if (this.cardState && Object.keys(this.cardState).length) {
      this.rerender();
    }
  }

  removeMe() {
    this.askForRemoval.emit();
  }

  rerender() {
    this.genericState = this.cardState;
    this.form.get('instrument').patchValue(this.genericState.instrument);
    this.form.get('type').patchValue(this.genericState.type);
    this.optionShown = false;
  }

  updateChanges() {
    this.genericState = this.form.value;
    this.optionShown = false;
    this.update.emit(this.genericState);
  }

  askForDuplicate() {
    this.duplicate.emit(this.genericState);
  }
}
