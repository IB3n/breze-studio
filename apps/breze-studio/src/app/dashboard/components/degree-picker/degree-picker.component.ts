import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'bz-degree-picker',
  templateUrl: './degree-picker.component.html',
  styleUrls: ['./degree-picker.component.scss'],
})
export class DegreePickerComponent {
  degrees = ['I', 'ii', 'iii', 'IV', 'V', 'vi', 'VII'];

  @Input()
  selectedDegree: string;

  @Output()
  degreeChange = new EventEmitter<string>();

  selectDegree(degree: string) {
    this.selectedDegree = degree;
    this.degreeChange.emit(degree);
  }
}
