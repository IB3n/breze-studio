import { GuitarLike } from '../../../core/instrument/guitar-like/guitar-like';
import { ChordDiagramJSON } from '../../../shared/components/diagram/chord-diagram';

export interface InstrumentState {
  instrument: GuitarLike;
  progression: Array<ChordDiagramJSON>;
}
