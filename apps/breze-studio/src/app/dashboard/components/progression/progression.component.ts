import {
  Component,
  Input,
  OnChanges,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { Draw } from 'tone';
import { GuitarLike } from '../../../core/instrument/guitar-like/guitar-like';
import { SampleService } from '../../../core/player/sample/sample.service';
import { TransportService } from '../../../core/player/transport/transport.service';
import { TheoryService } from '../../../core/theory.service';
import { NoteWithHeight } from '../../../interface/note-with-height';
import { DiagramComponent } from '../../../shared/components/diagram/diagram.component';

import { ModifiersService } from '../../services/modifiers/modifiers.service';
import { InstrumentState } from './instrument-progression-state';

@Component({
  selector: 'bz-progression',
  templateUrl: './progression.component.html',
  styleUrls: ['./progression.component.scss'],
})
export class ProgressionComponent implements OnChanges {
  @Input()
  instrument: GuitarLike;

  @Input()
  key: string;

  @Input()
  progression: Array<string>;

  @ViewChildren(DiagramComponent) diagrams: QueryList<DiagramComponent>;

  instrumentState: InstrumentState;
  chordProgression: Array<string>;

  constructor(
    private theoryService: TheoryService,
    private modifiersService: ModifiersService,
    private sampleService: SampleService,
    private transportService: TransportService
  ) {
    this.sampleService.loadSample();
  }

  ngOnChanges() {
    console.log(
      '[PROGRESSION-COMPONENT] input changed',
      this.key,
      this.progression
    );
    this.chordProgression = this.theoryService.progressionService.generateProgression(
      this.key,
      this.progression
    );
    this.chordProgression = this.modifiersService.applyToChordProgression(
      this.chordProgression
    );
    this.instrumentState = {
      instrument: this.instrument,
      progression: this.chordProgression.map((chord: string) => {
        return {
          chord,
          positions: this.instrument.getPosition(this.theoryService.chordService.generateChord(chord)),
        };
      }),
    };
  }

  clearActiveDiagrams() {
    this.diagrams.forEach((item: DiagramComponent) => {
      item.setPlayState(false);
    });
  }

  playProgression() {
    console.log('[PROGRESSION-COMPONENT] playing progression');
    this.sampleService.sampler.sync();
    this.diagrams.forEach((item: DiagramComponent, diagramIndex: number) => {
      const chordTimePosition = diagramIndex * 2 + (diagramIndex + 1) / 6;
      console.log('playing on ', '+' + chordTimePosition);
      Draw.schedule(() => {
        this.clearActiveDiagrams();
        item.setPlayState(true);
      }, '+' + chordTimePosition);
      item.chordConstruction.forEach(
        (note: NoteWithHeight, noteIndex: number) => {
          const ret = note.note + note.height;
          const duration = noteIndex / item.chordConstruction.size;
          const time = diagramIndex * 2 + duration / 6;
          this.transportService.scheduleOnce(() => {
            this.sampleService.sampler.triggerAttackRelease(ret, 2);
          }, '+' + time);
        }
      );
    });
  }
}
