import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgressionComponent } from './progression.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/compiler/src/core';
import { SampleService } from '../../../core/player/sample/sample.service';
import { TheoryService } from '../../../core/theory.service';

describe('ProgressionComponent', () => {
  let component: ProgressionComponent;
  let fixture: ComponentFixture<ProgressionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProgressionComponent],
      providers: [
        {
          provide: SampleService,
          useValue: {
            loadSample: jest.fn(() => undefined),
            sampler: {
              sync: jest.fn(() => undefined),
              triggerAttackRelease: jest.fn(() => undefined),
            },
          },
        },
        TheoryService
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgressionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
