import { Injectable } from '@angular/core';
import { ChordProgression } from '@breze-studio/music-theory';
import { List } from 'immutable';
import { BehaviorSubject } from 'rxjs';
import { ChordModifier } from '../../../interface/chord-modifier';

@Injectable({
  providedIn: 'root',
})
export class ModifiersService {
  modifiers$ = new BehaviorSubject<List<ChordModifier>>(List([]));

  get modifiers() {
    return this.modifiers$.getValue();
  }

  set modifiers(modifiers: List<ChordModifier>) {
    this.modifiers$.next(modifiers);
  }

  loadModifiers(modifiers?: Array<ChordModifier>) {
    if (!modifiers) {
      this.modifiers = List([]);
    } else {
      this.modifiers = List(modifiers);
    }
  }

  addChordModifier(modifier?: ChordModifier) {
    if (!modifier) {
      this.modifiers = this.modifiers.push({
        origin: '',
        target: '',
      });
    } else {
      this.modifiers = this.modifiers.push(modifier);
    }
  }

  removeModifier(index: number) {
    this.modifiers = this.modifiers.remove(index);
  }

  applyToChordProgression(
    chordProgression: ChordProgression
  ): ChordProgression {
    let _chordProgression = List(chordProgression);
    this.modifiers.forEach((chordModifier: ChordModifier) => {
      _chordProgression = _chordProgression.map((chord: string) => {
        return this.applyModifierToChord(chordModifier, chord);
      });
    });
    return Array.from(_chordProgression);
  }

  update(newModifier: ChordModifier, index: number) {
    this.modifiers = this.modifiers.set(index, newModifier);
  }

  applyModifierToChord(modifier: ChordModifier, chord: string) {
    return chord.replace(modifier.origin, modifier.target);
  }
}
