import { List } from 'immutable';
import { ChordModifier } from '../../../interface/chord-modifier';

import { ModifiersService } from './modifiers.service';

describe('ModifiersService', () => {
  let modifierService: ModifiersService;
  let _modifierAmAm7: ChordModifier;
  let _modifierAm7Bm: ChordModifier;
  let _initialState: List<any>;

  beforeEach(() => {
    _modifierAmAm7 = {
      origin: 'Am',
      target: 'Am7',
    };
    _modifierAm7Bm = {
      origin: 'Am7',
      target: 'Bm',
    };
    _initialState = List([]);
    modifierService = new ModifiersService();
  });

  describe('initial state', () => {
    it('modifiers is an empty list at service instanciation', () => {
      expect(modifierService.modifiers).toEqual(_initialState);
    });
  });

  describe('loadModifiers', () => {
    it('if loadModifiers receive null, modifiers is reset to an empty List', () => {
      modifierService.addChordModifier(_modifierAmAm7);
      modifierService.loadModifiers();
      expect(modifierService.modifiers).toEqual(_initialState);
    });
    it('loadModifiers load an array of modifiers into current state', () => {
      const _modifierAmAm7s = [_modifierAmAm7];
      modifierService.loadModifiers(_modifierAmAm7s);
      expect(modifierService.modifiers).toEqual(List(_modifierAmAm7s));
    });
  });

  describe('addChordModifier', () => {
    it('add a modifier to the current modifiers', () => {
      expect(modifierService.modifiers).toEqual(_initialState);
      modifierService.addChordModifier(_modifierAmAm7);
      expect(modifierService.modifiers).toEqual(List([_modifierAmAm7]));
    });
  });

  describe('removeModifiers', () => {
    it('does nothing if modifiers is empty', () => {
      modifierService.removeModifier(1);
      expect(modifierService.modifiers).toEqual(_initialState);
    });

    it('removes element at given index', () => {
      modifierService.loadModifiers([_modifierAmAm7]);
      modifierService.removeModifier(0);
      expect(modifierService.modifiers).toEqual(_initialState);
    });
  });

  describe('applyModifierToChord', () => {
    it('e2e : Am->Am7 works', () => {
      expect(
        modifierService.applyModifierToChord(_modifierAmAm7, 'Am')
      ).toEqual('Am7');
    });
  });

  describe('applyToChordProgression', () => {
    it('apply on more than on chord', () => {
      modifierService.addChordModifier(_modifierAmAm7);
      expect(
        modifierService.applyToChordProgression([
          _modifierAmAm7.origin,
          _modifierAmAm7.origin,
        ])
      ).toEqual([_modifierAmAm7.target, _modifierAmAm7.target]);
    });

    it('apply modifiers sequanetially by order of creation', () => {
      modifierService.addChordModifier(_modifierAmAm7);
      modifierService.addChordModifier(_modifierAm7Bm);
      expect(
        modifierService.applyToChordProgression([_modifierAmAm7.origin])
      ).toEqual([_modifierAm7Bm.target]);
    });

    it('if not modifier is present the chord progression is not changed', () => {
      const _chordProgression = ['A', 'Am7'];
      expect(
        modifierService.applyToChordProgression(_chordProgression)
      ).toEqual(_chordProgression);
    });
  });
});
