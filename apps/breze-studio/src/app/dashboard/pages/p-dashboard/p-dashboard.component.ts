import { Component, ViewChild } from '@angular/core';
import { saveAs } from 'file-saver';
import { List } from 'immutable';
import { Observable } from 'rxjs';
import { GuitarLikeBuilderService } from '../../../core/instrument/guitar-like/guitar-like.builder.service';
import { ChordModifier } from '../../../interface/chord-modifier';
import { DashboardStateV2 } from '../../../interface/dashboard-state-v2';
import { SaveState } from '../../../interface/save-state';

import { GenericCardState } from '../../components/generic-card/generic-card-state';
import { ModifiersService } from '../../services/modifiers/modifiers.service';

@Component({
  selector: 'bz-p-dashboard',
  templateUrl: './p-dashboard.component.html',
  styleUrls: ['./p-dashboard.component.scss'],
})
export class PDashboardComponent {
  cardsData = [];

  fileReader = new FileReader();

  modifiersShown = false;

  @ViewChild('loader')
  loaderInput: HTMLInputElement;

  modifiers$: Observable<List<ChordModifier>>;

  stateV2: DashboardStateV2 = {
    progression: [],
    root: 'A',
  };

  constructor(
    private guitarLikeBuilder: GuitarLikeBuilderService,
    private modifierService: ModifiersService
  ) {
    this.modifiers$ = this.modifierService.modifiers$;
    this.modifiers$.subscribe(() => {
      this.cardsData = Array.from(
        List(
          this.cardsData.map((element) => {
            this.guitarLikeBuilder.guitarLikeJSON = element.instrument;
            element.instrument = this.guitarLikeBuilder.build();
            return element;
          })
        )
      );
    });
  }

  addCard() {
    this.cardsData.push({});
  }

  reset() {
    this.cardsData = [];
    this.modifierService.loadModifiers();
  }

  dulpicateCard(genericState: any) {
    this.cardsData.push(genericState);
  }

  removeCard(index: number) {
    this.cardsData = this.cardsData.filter((_e, id) => id !== index);
  }

  handleCardUpdate(id: number, state: GenericCardState) {
    this.cardsData[id] = state;
  }

  saveState() {
    const name = 'dashboard_' + new Date().getTime() + '.brz';
    const _saveState: SaveState = {
      cardsData: this.cardsData,
      modifiers: Array.from(this.modifierService.modifiers),
    };
    const file = new Blob([JSON.stringify(_saveState)], {
      type: 'text/plain;charset=utf-8',
    });
    saveAs(file, name);
  }

  loadState($event) {
    const handleFileSelect = (event) => {
      this.fileReader.readAsText(event.target.files[0]);
    };
    this.fileReader.onload = (event) => {
      const textData = (event.target as any).result;
      const data: SaveState = JSON.parse(textData);
      this.modifierService.loadModifiers(data.modifiers);
      this.cardsData = data.cardsData.map((element) => {
        this.guitarLikeBuilder.guitarLikeJSON = element.instrument as any;
        element.instrument = this.guitarLikeBuilder.build();
        return element;
      });
      console.log('done loading');
    };
    handleFileSelect($event);
  }

  addModifier() {
    this.modifierService.addChordModifier();
  }

  updateModifier($event: ChordModifier, index: number) {
    this.modifierService.update($event, index);
  }

  removeModifier(i: number) {
    this.modifierService.removeModifier(i);
  }

  trackById(id: number) {
    return id;
  }
  showModifiers() {
    this.modifiersShown = true;
  }
  hideModifiers() {
    this.modifiersShown = false;
  }

  registerNewProgression(progression: Array<string>) {
    console.log(
      '[DASHBOARD] New progression came in',
      progression,
      'updating...'
    );
    this.stateV2.progression = progression;
  }

  registerNewRoot(root: string) {
    console.log('[DASHBOARD] New root came in', root, 'updating...');
    this.stateV2.root = root;
  }
}
