import { Pipe, PipeTransform } from '@angular/core';
import { NoteWithHeight } from '../../interface/note-with-height';

@Pipe({
  name: 'noteWithHeight',
})
export class NoteWithHeightPipe implements PipeTransform {
  transform(value: NoteWithHeight): string {
    return value.note + value.height;
  }
}
