import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Input,
  OnInit,
} from '@angular/core';
import { List } from 'immutable';
import { fromEvent } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { GuitarLike } from '../../../core/instrument/guitar-like/guitar-like';
import { InstrumentString } from '../../../core/instrument/string/instrument-string';
import { TheoryService } from '../../../core/theory.service';
import { DiagramDotPosition } from '../../../interface/diagram-dot-position';
import { DiagramMargin } from '../../../interface/diagram-margin';
import { NoteWithHeight } from '../../../interface/note-with-height';

@Component({
  selector: 'bz-diagram',
  templateUrl: './diagram.component.html',
  styleUrls: ['./diagram.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DiagramComponent implements OnInit {
  @Input()
  label: string;

  @Input()
  data: GuitarLike;

  @Input()
  positions: Array<number> = [];

  @Input()
  lazyLoad = false;

  displayed: boolean;

  margin: DiagramMargin;

  strings: List<DiagramDotPosition>;
  chordConstruction: List<NoteWithHeight>;
  height: number;
  width: number;
  innerHeight: number;
  innerWidth: number;
  verticalLines: Array<number>;
  fretDisplayed: number;
  fretSize: number;

  constructor(
    private el: ElementRef,
    private cdr: ChangeDetectorRef,
    private theoryService: TheoryService
  ) {
    this.margin = {
      top: 30,
      left: 20,
      right: 10,
      bottom: 12,
    };
    this.fretDisplayed = 6;
    this.height = 120;
    this.width = 120;
    this.innerHeight = this.height - this.margin.top - this.margin.bottom;
    this.innerWidth = this.width - this.margin.left - this.margin.right;
    this.fretSize = this.innerWidth / this.fretDisplayed;
    this.verticalLines = ' '
      .repeat(this.fretDisplayed)
      .split('')
      .map(
        (_, id) =>
          this.margin.left + (this.innerWidth * id) / this.fretDisplayed
      );

    const scrollSubscription = this.getScrollerScrollEvent().subscribe(() => {
      if (this.displayed) {
        scrollSubscription.unsubscribe();
      } else {
        this.displayed = this.isInView();
        this.cdr.markForCheck();
        if (this.displayed) {
          this.draw();
        }
      }
    });
  }
  isPlaying = false;
  setPlayState(playing: boolean) {
    this.isPlaying = playing;
    this.cdr.markForCheck();
  }

  ngOnInit() {
    this.displayed = this.lazyLoad ? this.isInView() : true;
    if (this.displayed) {
      this.draw();
    }
  }

  draw() {
    if (!this.data) {
      return;
    }
    const nbStrings = this.data.strings.size;
    this.strings = this.data.strings.map(
      (string: InstrumentString, idx: number) => {
        const hasPosition = typeof this.positions[idx] !== 'undefined';
        return {
          y: this.margin.top + (idx * this.innerHeight) / (nbStrings - 1),
          stringName: string.note,
          x: hasPosition
            ? this.positions[idx] === 0
              ? this.margin.left
              : this.margin.left +
                this.positions[idx] * this.fretSize -
                this.fretSize / 2
            : null,
          position: this.positions[idx],
        };
      }
    );
    this.buildChordConstruction();
  }

  getScrollerScrollEvent() {
    return fromEvent(window, 'scroll').pipe(debounceTime(20));
  }

  isInView() {
    const { top } = (
      this.el.nativeElement as HTMLElement
    ).getBoundingClientRect();
    const isInView = top < window.scrollY + window.outerHeight;
    return isInView;
  }

  buildChordConstruction() {
    this.chordConstruction = this.strings.map(
      (string: DiagramDotPosition, idx: number) => {
        return this.theoryService.noteService.getNoteWithHeightAndFreq(
          this.data.strings.get(idx).notes[string.position]
        );
      }
    );
  }
}
