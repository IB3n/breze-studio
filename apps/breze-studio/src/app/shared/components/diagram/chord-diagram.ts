export interface ChordDiagramJSON {
  chord: string;
  positions: Array<number>;
}
