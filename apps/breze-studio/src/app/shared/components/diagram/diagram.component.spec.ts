import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TheoryService } from '../../../core/theory.service';
import { NoteWithHeightPipe } from '../../pipes/note-with-height.pipe';

import { DiagramComponent } from './diagram.component';

describe('DiagramComponent', () => {
  let component: DiagramComponent;
  let fixture: ComponentFixture<DiagramComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DiagramComponent, NoteWithHeightPipe],
      providers: [TheoryService]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiagramComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
