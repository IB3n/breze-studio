import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HarmonicaDiagramComponent } from './harmonica-diagram.component';

describe('HarmonicaDiagramComponent', () => {
  let component: HarmonicaDiagramComponent;
  let fixture: ComponentFixture<HarmonicaDiagramComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HarmonicaDiagramComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HarmonicaDiagramComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
