import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'bz-harmonica-diagram',
  templateUrl: './harmonica-diagram.component.html',
  styleUrls: ['./harmonica-diagram.component.scss'],
})
export class HarmonicaDiagramComponent implements OnInit {
  nbHoles = 10;

  height: number;
  width: number;
  separatorWidth: number;
  verticalLines: Array<any>;
  textBoxes: Array<any>;
  boxHeight: number;

  constructor() {
    this.height = 60;
    this.width = 300;
    this.boxHeight = this.height / 3;
  }

  ngOnInit() {
    console.log('changes');
    this.separatorWidth = this.width / this.nbHoles;
    this.buildVerticalLines();
    this.buildBoxes();
  }

  private buildVerticalLines() {
    this.verticalLines = ' '
      .repeat(this.nbHoles - 1)
      .split('')
      .map((_item, idx) => {
        const x = this.separatorWidth * (idx + 1);
        return {
          x1: x,
          y1: 0,
          x2: x,
          y2: this.height,
        };
      });
  }

  private buildBoxes() {
    this.textBoxes = ' '
      .repeat(this.nbHoles)
      .split('')
      .map((_item, idx) => {
        return {
          x: idx * this.separatorWidth + this.separatorWidth / 2,
          y: this.boxHeight + this.boxHeight / 2,
          textContent: idx + 1,
        };
      });
  }
}
