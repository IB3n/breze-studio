import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { DiagramComponent } from './components/diagram/diagram.component';
import { HarmonicaDiagramComponent } from './components/harmonica-diagram/harmonica-diagram.component';
import { NoteWithHeightPipe } from './pipes/note-with-height.pipe';

const ioDeclarations = [
  DiagramComponent,
  HarmonicaDiagramComponent,
  NoteWithHeightPipe,
];

@NgModule({
  declarations: ioDeclarations,
  exports: ioDeclarations,
  imports: [CommonModule],
})
export class SharedModule {}
