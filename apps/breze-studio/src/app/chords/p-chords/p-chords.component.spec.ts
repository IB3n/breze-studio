import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TheoryService } from '../../core/theory.service';

import { PChordsComponent } from './p-chords.component';

describe('PChordsComponent', () => {
  let component: PChordsComponent;
  let fixture: ComponentFixture<PChordsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PChordsComponent],
      providers: [TheoryService],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PChordsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
