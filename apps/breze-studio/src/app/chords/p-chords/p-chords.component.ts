import { Component } from '@angular/core';
import { List } from 'immutable';
import { GuitarLike } from '../../core/instrument/guitar-like/guitar-like';
import { InstrumentService } from '../../core/instrument/instrument.service';
import { NoteChords } from '../../interface/chord-list';
import { chordTypes, MinimalDictArray } from "@breze-studio/music-theory";
import { TheoryService } from '../../core/theory.service';

@Component({
  selector: 'bz-p-chords',
  templateUrl: './p-chords.component.html',
  styleUrls: ['./p-chords.component.scss'],
})
export class PChordsComponent {
  notes = MinimalDictArray;

  chordTypes = chordTypes;

  chordList: NoteChords;

  instruments: List<GuitarLike>;
  currentInstrument: GuitarLike;
  currentNote: string;

  constructor(
    private instrumentsService: InstrumentService,
    private theoryService: TheoryService
  ) {
    this.instruments = this.instrumentsService.instruments;
    this.selectInstrument(this.instruments.get(0));
    this.selectNote(this.notes[0]);
  }

  selectInstrument(instrument: GuitarLike) {
    this.currentInstrument = instrument;
    this.buildChordList();
  }

  selectNote(note: string) {
    this.currentNote = note;
    this.buildChordList();
  }

  buildChordList() {
    if (this.currentNote && this.currentInstrument) {
      this.chordList = {
        note: this.currentNote,
        chords: this.chordTypes.map((chordType: string) => {
          const _chord = this.currentNote + chordType;
          return {
            name: _chord,
            positions: this.currentInstrument.getPosition(
              this.theoryService.chordService.generateChord(_chord)
            ),
          };
        }),
      };
    }
  }
}
