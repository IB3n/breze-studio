import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';
import { ChordsRoutingModule } from './chords-routing.module';
import { PChordsComponent } from './p-chords/p-chords.component';

@NgModule({
  declarations: [PChordsComponent],
  imports: [CommonModule, ChordsRoutingModule, SharedModule],
})
export class ChordsModule {}
