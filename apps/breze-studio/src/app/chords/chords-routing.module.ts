import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PChordsComponent } from './p-chords/p-chords.component';

const routes: Routes = [
  {
    path: '',
    component: PChordsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChordsRoutingModule {}
