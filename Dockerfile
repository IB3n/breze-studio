FROM node:16-alpine

WORKDIR /usr/src/breze

COPY package*.json ./
COPY decorate-angular-cli.js ./

RUN npm ci -ddd
RUN apk add --no-cache rsync