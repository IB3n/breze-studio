import { NoteService, ProgressionService, ScaleService } from '.';
import { ChordService } from './chord';

export interface BrezeMusicTheoryParams {
  chordService?: ChordService;
  noteService?: NoteService;
  scaleService?: ScaleService;
  progressionService?: ProgressionService;
}

export class BrezeMusicTheory {

  chordService: ChordService;
  noteService: NoteService;
  scaleService: ScaleService;
  progressionService: ProgressionService;

  constructor(params: BrezeMusicTheoryParams = {}) {
    this.noteService = params.noteService || new NoteService();
    this.chordService = params.chordService || new ChordService(this.noteService);
    this.scaleService = params.scaleService || new ScaleService(this.noteService, this.chordService);
    this.progressionService = params.progressionService || new ProgressionService(this.noteService);
  }

}


