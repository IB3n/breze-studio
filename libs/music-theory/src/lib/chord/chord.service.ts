import { NoteService } from '../note/note.service';


export class ChordService {
  constructor(
    private noteService: NoteService
  ) {}

  minorChord = (note) => [
    note,
    this.noteService.get3m(note),
    this.noteService.get5J(note),
  ];
  majorChord = (note) => [
    note,
    this.noteService.get3M(note),
    this.noteService.get5J(note),
  ];

  sus4Chord = (note) => [
    note,
    this.noteService.get4J(note),
    this.noteService.get5J(note),
  ];

  sus2Chord = (note) => [
    note,
    this.noteService.get2M(note),
    this.noteService.get5J(note),
  ];

  minor7Chord = (note) => [
    note,
    this.noteService.get3m(note),
    this.noteService.get5J(note),
    this.noteService.get7m(note),
  ];

  major7Chord = (note) => [
    // Amaj7
    note,
    this.noteService.get3M(note),
    this.noteService.get5J(note),
    this.noteService.get7M(note),
  ];

  major6Chord = (note) => [
    note,
    this.noteService.get3M(note),
    this.noteService.get5J(note),
    this.noteService.get6M(note),
  ];

  minor6Chord = (note) => [
    note,
    this.noteService.get3m(note),
    this.noteService.get5J(note),
    this.noteService.get6M(note),
  ];

  augChord = (note) => [
    note,
    this.noteService.get3M(note),
    this.noteService.get5aug(note),
  ];

  dimChord = (note) => [
    note,
    this.noteService.get3m(note),
    this.noteService.get5dim(note),
  ];

  dom7Chord = (note) => [
    // A7
    note,
    this.noteService.get3M(note),
    this.noteService.get5J(note),
    this.noteService.get7m(note),
  ];

  minor9Chord = (note) => [
    note,
    this.noteService.get3M(note),
    this.noteService.get5J(note),
    this.noteService.get7m(note),
    this.noteService.get9m(note),
  ];

  major9Chord = (note) => [
    note,
    this.noteService.get3M(note),
    this.noteService.get5J(note),
    this.noteService.get7m(note),
    this.noteService.get9M(note),
  ];

  add9Chord = (note) => [
    note,
    this.noteService.get3M(note),
    this.noteService.get5J(note),
    this.noteService.get9M(note),
  ];

  septSus4Chord = (note) => [
    note,
    this.noteService.get4J(note),
    this.noteService.get5J(note),
    this.noteService.get7m(note),
  ];

  generateChord(chordName: string) {
    if (!this.isValidChord(chordName)) {
      throw new Error(`the chord ${chordName} is not valid`);
    }
    let _chord = chordName;
    const _chordLength = _chord.length;

    if (_chord.endsWith('7sus4')) {
      _chord = _chord.substring(0, _chordLength - 5);
      return this.septSus4Chord(_chord);
    }

    if (_chord.endsWith('sus4')) {
      _chord = _chord.substring(0, _chordLength - 4);
      return this.sus4Chord(_chord);
    }
    if (_chord.endsWith('sus2')) {
      _chord = _chord.substring(0, _chordLength - 4);
      return this.sus2Chord(_chord);
    }
    if (_chord.endsWith('aug')) {
      _chord = _chord.substring(0, _chordLength - 3);
      return this.augChord(_chord);
    }
    if (_chord.endsWith('dim')) {
      _chord = _chord.substring(0, _chordLength - 3);
      return this.dimChord(_chord);
    }

    if (chordName.endsWith('6')) {
      if (chordName.endsWith('m6')) {
        const chordStrLength = chordName.length;
        return this.minor6Chord(chordName.substring(0, chordStrLength - 2));
      } else {
        const chordStrLength = chordName.length;
        return this.major6Chord(chordName.substring(0, chordStrLength - 1));
      }
    }

    if (chordName.endsWith('9')) {
      if (chordName.endsWith('m9')) {
        const chordStrLength = chordName.length;
        return this.minor9Chord(chordName.substring(0, chordStrLength - 2));
      } else if (chordName.endsWith('add9')) {
        const chordStrLength = chordName.length;
        return this.add9Chord(chordName.substring(0, chordStrLength - 4));
      } else {
        const chordStrLength = chordName.length;
        return this.major9Chord(chordName.substring(0, chordStrLength - 1));
      }
    }

    if (chordName.endsWith('7')) {
      if (chordName.endsWith('m7')) {
        // build a 7th minor
        const chordStrLength = chordName.length;
        return this.minor7Chord(chordName.substring(0, chordStrLength - 2));
      } else if (chordName.endsWith('maj7')) {
        // build a 7th maj
        const chordStrLength = chordName.length;
        return this.major7Chord(chordName.substring(0, chordStrLength - 4));
      } else {
        // build a dom7th
        const chordStrLength = chordName.length;
        return this.dom7Chord(chordName.substring(0, chordStrLength - 1));
      }
    }
    if (chordName.endsWith('m')) {
      const chordStrLength = chordName.length;
      return this.minorChord(chordName.substring(0, chordStrLength - 1));
    } else {
      return this.majorChord(chordName);
    }
  }

  isValidChord(chord: string) {
    let _chord = chord;
    let chordLength = _chord.length;
    if (chord.endsWith('sus4') || chord.endsWith('sus2')) {
      _chord = chord.substring(0, chordLength - 4);
      chordLength = _chord.length;
    }
    if (chord.endsWith('aug') || chord.endsWith('dim')) {
      _chord = chord.substring(0, chordLength - 3);
      chordLength = _chord.length;
    }
    if (_chord.endsWith('7') || chord.endsWith('6') || chord.endsWith('9')) {
      _chord = _chord.substring(0, chordLength - 1);
      chordLength = _chord.length;
    }
    if (_chord.endsWith('maj') || _chord.endsWith('add')) {
      _chord = chord.substring(0, chordLength - 3);
      chordLength = _chord.length;
    }
    if (_chord.endsWith('m')) {
      _chord = chord.substring(0, chordLength - 1);
      chordLength = _chord.length;
    }
    return this.noteService.isValid(_chord);
  }
}
