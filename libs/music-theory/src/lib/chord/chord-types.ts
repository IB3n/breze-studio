export const chordTypes = [
  '', // Means major
  'm',
  'aug',
  'dim',
  '7',
  'm7',
  'maj7',
  '6',
  'm6',
  'add9',
  'm9',
  '9',
  'sus2',
  'sus4',
  '7sus4',
];
