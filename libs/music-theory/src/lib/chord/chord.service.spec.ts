import { NoteService } from '../note/note.service';
import { ChordService } from './chord.service';

describe('ChordService', () => {
  let chord: ChordService;
  let noteService: NoteService;

  beforeEach(() => {
    noteService = new NoteService();
    chord = new ChordService(noteService);
  });

  describe('generateChord', () => {
    it('returns an Array of string', () => {
      expect(chord.generateChord('A') instanceof Array).toBe(true);
    });

    it('check if the note is valid', () => {
      const isValidSpy = jest.spyOn(noteService, 'isValid');
      chord.generateChord('A');
      expect(isValidSpy).toHaveBeenCalled();
    });

    it('calls minorChord if minor is asked', () => {
      const minorChordSpy = jest.spyOn(chord, 'minorChord');
      chord.generateChord('Am');
      expect(minorChordSpy).toHaveBeenCalled();
    });

    it('calls majorChord if major is asked', () => {
      const majorChordSpy = jest.spyOn(chord, 'majorChord');
      chord.generateChord('A');
      expect(majorChordSpy).toHaveBeenCalled();
    });
  });

  describe('minorChord', () => {
    it('use a 3m and a 5J', () => {
      const spy3m = jest.spyOn(noteService, 'get3m');
      const spy5J = jest.spyOn(noteService, 'get5J');
      chord.generateChord('Am');
      expect(spy3m).toHaveBeenCalledWith('A');
      expect(spy5J).toHaveBeenCalledWith('A');
    });
  });

  describe('majorChord', () => {
    it('use a 3M and a 5J', () => {
      const spy3M = jest.spyOn(noteService, 'get3M');
      const spy5J = jest.spyOn(noteService, 'get5J');
      chord.generateChord('A');
      expect(spy3M).toHaveBeenCalledWith('A');
      expect(spy5J).toHaveBeenCalledWith('A');
    });
  });

  describe('dom7Chord', () => {
    it('use a 3M, a 5J and a 7m', () => {
      const spy3M = jest.spyOn(noteService, 'get3M');
      const spy5J = jest.spyOn(noteService, 'get5J');
      const spy7m = jest.spyOn(noteService, 'get7m');
      chord.generateChord('A7');
      expect(spy3M).toHaveBeenCalledWith('A');
      expect(spy5J).toHaveBeenCalledWith('A');
      expect(spy7m).toHaveBeenCalledWith('A');
    });
    it('e2e : Cdom7 (also C7) is C E G A#', () => {
      // Cdom7 (also C7) is C E G A#
      expect(chord.generateChord('C7')).toEqual(['C', 'E', 'G', 'A#']);
    });
  });

  describe('augChord', () => {
    it('use a 3M, a 5aug', () => {
      const spy3M = jest.spyOn(noteService, 'get3M');
      const spay5Aug = jest.spyOn(noteService, 'get5aug');
      chord.generateChord('Aaug');
      expect(spy3M).toHaveBeenCalledWith('A');
      expect(spay5Aug).toHaveBeenCalledWith('A');
    });
    it('e2e : Caug  is C E G#', () => {
      expect(chord.generateChord('Caug')).toEqual(['C', 'E', 'G#']);
    });
  });

  describe('dimChord', () => {
    it('use a 3M, a 5dim', () => {
      const spy3m = jest.spyOn(noteService, 'get3m');
      const spay5Dim = jest.spyOn(noteService, 'get5dim');
      chord.generateChord('Adim');
      expect(spy3m).toHaveBeenCalledWith('A');
      expect(spay5Dim).toHaveBeenCalledWith('A');
    });
    it('e2e : Cdim is C D# F#', () => {
      // C E♭ G♭
      expect(chord.generateChord('Cdim')).toEqual(['C', 'D#', 'F#']);
    });
  });

  describe('6Chord', () => {
    it('use a 3M, a 5dim', () => {
      const spy3m = jest.spyOn(noteService, 'get3M');
      const spy5Dim = jest.spyOn(noteService, 'get5J');
      const spy6M = jest.spyOn(noteService, 'get6M');
      chord.generateChord('A6');
      expect(spy3m).toHaveBeenCalledWith('A');
      expect(spy5Dim).toHaveBeenCalledWith('A');
      expect(spy6M).toHaveBeenCalledWith('A');
    });
    it('e2e : C6 is C E G A', () => {
      // C E♭ G♭
      expect(chord.generateChord('C6')).toEqual(['C', 'E', 'G', 'A']);
    });
  });

  describe('minor6Chord', () => {
    it('use a 3m, a 5J, 6M', () => {
      const spy3m = jest.spyOn(noteService, 'get3m');
      const spy5Dim = jest.spyOn(noteService, 'get5J');
      const spy6M = jest.spyOn(noteService, 'get6M');
      chord.generateChord('Am6');
      expect(spy3m).toHaveBeenCalledWith('A');
      expect(spy5Dim).toHaveBeenCalledWith('A');
      expect(spy6M).toHaveBeenCalledWith('A');
    });
    it('e2e : Cm6 is C D# G A', () => {
      // C E♭ G♭
      expect(chord.generateChord('Cm6')).toEqual(['C', 'D#', 'G', 'A']);
    });
  });

  describe('minor9Chord', () => {
    it('use a 3M, a 5J, 7m and 9m', () => {
      const spy3M = jest.spyOn(noteService, 'get3M');
      const spy5J = jest.spyOn(noteService, 'get5J');
      const spy7m = jest.spyOn(noteService, 'get7m');
      const spy9m = jest.spyOn(noteService, 'get9m');
      chord.generateChord('Am9');
      expect(spy3M).toHaveBeenCalledWith('A');
      expect(spy5J).toHaveBeenCalledWith('A');
      expect(spy7m).toHaveBeenCalledWith('A');
      expect(spy9m).toHaveBeenCalledWith('A');
    });
    it('e2e : Gm9 is G B D F G#', () => {
      // sol, si, ré, fa, lab
      expect(chord.generateChord('Gm9')).toEqual(['G', 'B', 'D', 'F', 'G#']);
    });
  });

  describe('minor9Chord', () => {
    it('use a 3M, a 5J, 7m and 9m', () => {
      const spy3M = jest.spyOn(noteService, 'get3M');
      const spy5J = jest.spyOn(noteService, 'get5J');
      const spy7m = jest.spyOn(noteService, 'get7m');
      const spy9m = jest.spyOn(noteService, 'get9m');
      chord.generateChord('Am9');
      expect(spy3M).toHaveBeenCalledWith('A');
      expect(spy5J).toHaveBeenCalledWith('A');
      expect(spy7m).toHaveBeenCalledWith('A');
      expect(spy9m).toHaveBeenCalledWith('A');
    });
    it('e2e : Gm9 is G B D F G#', () => {
      // sol, si, ré, fa, lab
      expect(chord.generateChord('Gm9')).toEqual(['G', 'B', 'D', 'F', 'G#']);
    });
  });

  describe('add9Chord', () => {
    it('use a 3M, a 5J, a 9M', () => {
      const spy3M = jest.spyOn(noteService, 'get3M');
      const spy5J = jest.spyOn(noteService, 'get5J');
      const spy9M = jest.spyOn(noteService, 'get9M');
      chord.generateChord('Aadd9');
      expect(spy3M).toHaveBeenCalledWith('A');
      expect(spy5J).toHaveBeenCalledWith('A');
      expect(spy9M).toHaveBeenCalledWith('A');
    });
    it('e2e : Cadd9 is C E G D', () => {
      // sol, si, ré, fa, lab
      expect(chord.generateChord('Cadd9')).toEqual(['C', 'E', 'G', 'D']);
    });
  });

  describe('major9Chord', () => {
    it('use a 3M, a 5J, 7m and 9m', () => {
      const spy3M = jest.spyOn(noteService, 'get3M');
      const spy5J = jest.spyOn(noteService, 'get5J');
      const spy7m = jest.spyOn(noteService, 'get7m');
      const spy9M = jest.spyOn(noteService, 'get9M');
      chord.generateChord('A9');
      expect(spy3M).toHaveBeenCalledWith('A');
      expect(spy5J).toHaveBeenCalledWith('A');
      expect(spy7m).toHaveBeenCalledWith('A');
      expect(spy9M).toHaveBeenCalledWith('A');
    });
    it('e2e : G9 is G B D F A', () => {
      // sol, si, ré, fa, la
      expect(chord.generateChord('G9')).toEqual(['G', 'B', 'D', 'F', 'A']);
    });
  });

  describe('septSus4Chord', () => {
    it('use a 4J, a 5J, 7m', () => {
      const spy4J = jest.spyOn(noteService, 'get4J');
      const spy5J = jest.spyOn(noteService, 'get5J');
      const spy7m = jest.spyOn(noteService, 'get7m');
      chord.generateChord('A7sus4');
      expect(spy4J).toHaveBeenCalledWith('A');
      expect(spy5J).toHaveBeenCalledWith('A');
      expect(spy7m).toHaveBeenCalledWith('A');
    });
    it('e2e : A#7sus4 is A# D# F G#', () => {
      expect(chord.generateChord('A#7sus4')).toEqual(['A#', 'D#', 'F', 'G#']);
    });
  });

  describe('isValidChord', () => {
    it('works on valid notes', () => {
      expect(chord.isValidChord('A#')).toBe(true);
      expect(chord.isValidChord('A')).toBe(true);
    });
    it('works on minor chords', () => {
      expect(chord.isValidChord('Am')).toBe(true);
    });
    it('works on dominant 7th chords', () => {
      expect(chord.isValidChord('A7')).toBe(true);
    });
    it('works on minor 7th chords', () => {
      expect(chord.isValidChord('Am7')).toBe(true);
    });
    it('works on major 7th chords', () => {
      expect(chord.isValidChord('Amaj7')).toBe(true);
    });
  });
});
