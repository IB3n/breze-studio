import { NoteService } from '../note/note.service';
import { ChordProgression } from './chord-progression';

export class ProgressionService {
  constructor(private noteService: NoteService) {}

  applyDegreeToNote(degree: string, note: string) {
    switch (degree) {
      case 'I':
        return this.noteService.get1J(note);
      case 'I7':
        return this.noteService.get1J(note) + '7'; // TODO build this
      case 'ii':
        return this.noteService.get2M(note) + 'm';
      case 'ii7':
        return this.noteService.get2M(note) + 'm7'; // TODO build this
      case 'II':
        return this.noteService.get2M(note);
      case 'III':
        return this.noteService.get3M(note);
      case 'IV':
        return this.noteService.get4J(note);
      case 'iii':
        return this.noteService.get3M(note) + 'm';
      case 'V':
        return this.noteService.get5J(note);
      case 'V7':
        return this.noteService.get5J(note) + '7'; // TODO build this
      case 'VI':
        return this.noteService.get6M(note);
      case 'vi':
        return this.noteService.get6M(note) + 'm';
      case 'VII':
        return this.noteService.get7M(note);
      case 'vii':
        return this.noteService.get7m(note);
      default:
        throw new Error(`could not apply degree ${degree} to note ${note}`);
    }
  }

  generateProgression(
    key: string,
    progression: Array<string>
  ): ChordProgression {
    return progression.map((degree) => this.applyDegreeToNote(degree, key));
  }
}
