export const defaultProgressions = [
  ['I', 'vi', 'iii', 'VII'],
  ['I', 'V', 'vi', 'IV'],
  ['I', 'vi', 'IV', 'V'],
  ['I', 'V', 'vi', 'iii', 'IV', 'I', 'IV', 'V'],
  ['ii', 'IV', 'V'],
  ['I', 'IV', 'V', 'IV'],
  ['V', 'IV', 'I'],
  ['vi', 'IV', 'I', 'V'],
  ['vi', 'V', 'IV', 'iii'],
];
