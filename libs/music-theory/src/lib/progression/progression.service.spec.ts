import { NoteService } from '../note/note.service';
import { ProgressionService } from './progression.service';

describe('ProgressionService', () => {
  let progressionService: ProgressionService;
  let noteService: NoteService;
  beforeEach(() => {
    noteService = new NoteService();
    progressionService = new ProgressionService(noteService);
  });

  describe('generateProgression', () => {
    it('returns an array of notes', () => {
      expect(
        progressionService.generateProgression('A', [
          'I',
          'V',
          'vi',
          'IV',
        ]) instanceof Array
      ).toBe(true);
    });

    it('calls applyDegreeToNote for evey degree', () => {
      const spy = jest.spyOn(progressionService, 'applyDegreeToNote');
      progressionService.generateProgression('A', ['I', 'V', 'vi', 'IV']);
      expect(spy).toHaveBeenCalledTimes(4);
    });

    it('calls applyDegreeToNote with the key', () => {
      const spy = jest.spyOn(progressionService, 'applyDegreeToNote');
      progressionService.generateProgression('A', ['I']);
      expect(spy).toHaveBeenCalledWith('I', 'A');
    });

    it('e2e : result seems correct for only major', () => {
      const result = progressionService.generateProgression('G', [
        'I',
        'IV',
        'V',
        'IV',
      ]);
      expect(result).toEqual(['G', 'C', 'D', 'C']);
    });

    it('e2e : result seems correct for major + minor', () => {
      const result = progressionService.generateProgression('G', [
        'I',
        'V',
        'vi',
        'IV',
      ]);
      expect(result).toEqual(['G', 'D', 'Em', 'C']);
    });
  });

  describe('applyDegreeToNote', () => {
    it('returns a new Note', () => {
      expect(typeof progressionService.applyDegreeToNote('I', 'A')).toBe(
        'string'
      );
    });
    it('return the prime when degree I is applied', () => {
      const noteSpy = jest.spyOn(noteService, 'get1J').mockReturnValue('^^');
      const result = progressionService.applyDegreeToNote('I', 'C');
      expect(noteSpy).toHaveBeenCalledWith('C');
      expect(result).toBe('^^');
    });

    it('return the 2M as a minor chord when degree ii is applied', () => {
      const noteSpy = jest.spyOn(noteService, 'get2M').mockReturnValue('^^');
      const result = progressionService.applyDegreeToNote('ii', 'C');
      expect(noteSpy).toHaveBeenCalledWith('C');
      expect(result).toBe('^^m');
    });

    it('return the 2M as a major chord when degree II is applied', () => {
      const noteSpy = jest.spyOn(noteService, 'get2M').mockReturnValue('^^');
      const result = progressionService.applyDegreeToNote('II', 'C');
      expect(noteSpy).toHaveBeenCalledWith('C');
      expect(result).toBe('^^');
    });

    it('return the 3M as a minor chord when degree iii is applied', () => {
      const noteSpy = jest.spyOn(noteService, 'get3M').mockReturnValue('^^');
      const result = progressionService.applyDegreeToNote('iii', 'C');
      expect(noteSpy).toHaveBeenCalledWith('C');
      expect(result).toBe('^^m');
    });

    // iii('G') = Bm
    it(`e2e : iii('G') = Bm`, () => {
      expect(progressionService.applyDegreeToNote('iii', 'G')).toBe('Bm');
    });

    it('return the 3M when degree III is applied', () => {
      const noteSpy = jest.spyOn(noteService, 'get3M').mockReturnValue('^^');
      const result = progressionService.applyDegreeToNote('III', 'C');
      expect(noteSpy).toHaveBeenCalledWith('C');
      expect(result).toBe('^^');
    });
    it('return the 4J when degree IV is applied', () => {
      const noteSpy = jest.spyOn(noteService, 'get4J').mockReturnValue('^^');
      const result = progressionService.applyDegreeToNote('IV', 'C');
      expect(noteSpy).toHaveBeenCalledWith('C');
      expect(result).toBe('^^');
    });
    it('returns the 5J when degree V is applied', () => {
      const noteSpy = jest.spyOn(noteService, 'get5J').mockReturnValue('^^');
      const result = progressionService.applyDegreeToNote('V', 'G');
      expect(noteSpy).toHaveBeenCalledWith('G');
      expect(result).toBe('^^');
    });
    it('returns the 6M when degree VI is applied', () => {
      const noteSpy = jest.spyOn(noteService, 'get6M').mockReturnValue('^^');
      const result = progressionService.applyDegreeToNote('VI', 'G');
      expect(noteSpy).toHaveBeenCalledWith('G');
      expect(result).toBe('^^');
    });
    it('returns the 6M as a minor chord when degree vi is applied', () => {
      const noteSpy = jest.spyOn(noteService, 'get6M').mockReturnValue('^^');
      const result = progressionService.applyDegreeToNote('vi', 'G');
      expect(noteSpy).toHaveBeenCalledWith('G');
      expect(result).toBe('^^m');
    });
    it(`e2e : vi('G') = 'Em'`, () => {
      expect(progressionService.applyDegreeToNote('vi', 'G')).toBe('Em');
    });
  });
});
