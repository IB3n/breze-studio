import { Map } from 'immutable';

export const AnglicanNotesToMinimalTable = Map({
  'A♭': 'G#',
  A: 'A',
  'A#': 'A#',
  'B♭': 'A#',
  B: 'B',
  C: 'C',
  'C#': 'C#',
  'D♭': 'C#',
  D: 'D',
  'D#': 'D#',
  'E♭': 'D#',
  E: 'E',
  F: 'F',
  'F#': 'F#',
  'G♭': 'F#',
  G: 'G',
  'G#': 'G#',
});
