export interface NoteWithHeight {
  note: string;
  height: number;
  frequency?: number;
}
