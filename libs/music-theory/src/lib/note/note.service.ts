import { AnglicanNotesDict } from './anglican_notes';
import { MinimalDictArray } from './anglican_notes_minimal';
import { AnglicanNotesToMinimalTable } from './anglican_notes_to_minimal_table';
import { NoteWithHeight } from './note-with-height';
import { noteToFreq } from './note_to_freq';

export class NoteService {
  get1J = (note) => this.getStepsAfter(note, 0);
  get2m = (note) => this.getStepsAfter(note, 1);
  get2M = (note) => this.getStepsAfter(note, 2);
  get3m = (note) => this.getStepsAfter(note, 3);
  get3M = (note) => this.getStepsAfter(note, 4);
  get4J = (note) => this.getStepsAfter(note, 5);
  get4aug = (note) => this.getStepsAfter(note, 6);
  get5dim = (note) => this.getStepsAfter(note, 6);
  get5J = (note) => this.getStepsAfter(note, 7);
  get5aug = (note) => this.getStepsAfter(note, 8);
  get6m = (note) => this.getStepsAfter(note, 8);
  get6M = (note) => this.getStepsAfter(note, 9);
  get7m = (note) => this.getStepsAfter(note, 10);
  get7M = (note) => this.getStepsAfter(note, 11);
  get9m = (note) => this.getStepsAfter(note, 13);
  get9M = (note) => this.getStepsAfter(note, 14);

  isValid(note: string | NoteWithHeight) {
    if (typeof note !== 'string') {
      return AnglicanNotesDict.has(note.note);
    }
    return AnglicanNotesDict.has(note);
  }

  getMinimalNote(note: string) {
    if (!this.isValid) {
      throw new Error(`getMinimalNote - the note ${note} is not valid`);
    }
    return AnglicanNotesToMinimalTable.get(note);
  }

  getStepsAfter(note: string, steps: number) {
    if (!this.isValid(note)) {
      throw new Error(`getStepsAfter - the note ${note} is not valid`);
    }
    const minimalDictNoteIndex = MinimalDictArray.indexOf(note);
    const minimalDictNoteLength = MinimalDictArray.length;
    const target = (minimalDictNoteIndex + steps) % minimalDictNoteLength;
    return MinimalDictArray[target];
  }

  getDistanceFromScaleStart(note: string) {
    const CPosition = MinimalDictArray.indexOf('C');
    const notePosition = MinimalDictArray.indexOf(note);
    const distance = notePosition - CPosition;
    return distance >= 0 ? distance : 12 + distance;
  }

  getStepsAfterWithHeight(note: NoteWithHeight, steps: number): NoteWithHeight {
    if (!this.isValid(note)) {
      throw new Error(`getStepsAfter - the note ${note} is not valid`);
    }
    // target note
    const finalNote = {} as NoteWithHeight;
    finalNote.note = this.getStepsAfter(note.note, steps);
    const distanceFromC = this.getDistanceFromScaleStart(note.note);
    const isOnDifferentOctave = distanceFromC + steps >= 12;
    let nbOctave;
    if (!isOnDifferentOctave) {
      nbOctave = 0;
    } else {
      nbOctave = Math.floor((distanceFromC + steps) / 12);
    }
    finalNote.height = note.height + nbOctave;
    return finalNote;
  }

  getNoteWithHeightAndFreq(note: NoteWithHeight): NoteWithHeight {
    return {
      note: note.note,
      frequency: noteToFreq[note.note][note.height],
      height: note.height,
    };
  }
}
