import { Set } from 'immutable';

export class NoteJSON {
  quinte: string;
  fondamental: string;
  tierce: string;
}

export const AnglicanNotesDict = Set([
  'A♭',
  'A',
  'A#',
  'B♭',
  'B',
  'C',
  'C#',
  'D♭',
  'D',
  'D#',
  'E♭',
  'E',
  'F',
  'F#',
  'G♭',
  'G',
  'G#',
]);
