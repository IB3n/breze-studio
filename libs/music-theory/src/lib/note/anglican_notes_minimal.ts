import { Set } from 'immutable';

export const MinimalDictArray = [
  'A', // 0
  'A#',
  'B',
  'C',
  'C#',
  'D', // 5
  'D#',
  'E',
  'F',
  'F#',
  'G', // 10
  'G#',
];

export const AnglicanMinimalDict = Set(MinimalDictArray);
