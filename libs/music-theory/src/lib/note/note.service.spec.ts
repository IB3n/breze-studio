import { NoteWithHeight } from './note-with-height';
import { NoteService } from './note.service';

describe('NoteService', () => {
  let noteService: NoteService;

  beforeEach(() => {
    noteService = new NoteService();
  });

  describe('isValid', () => {
    it('returns a boolean', () => {
      expect(typeof noteService.isValid('A')).toBe('boolean');
    });
    it('returns true if is a valid anglican note', () => {
      ['A', 'B', 'C', 'D', 'E', 'F', 'G'].forEach((anglicanNote) => {
        expect(noteService.isValid(anglicanNote)).toBe(true);
      });
    });
    it('returns false if not a valid anglican note', () => {
      expect(noteService.isValid('H')).toBe(false);
    });
    it('works for sharp notes', () => {
      expect(noteService.isValid('A#')).toBe(true);
    });
    it('works for flat notes', () => {
      expect(noteService.isValid('A♭')).toBe(true);
    });
    it('handles basic note limit case', () => {
      expect(noteService.isValid('E#')).toBe(false);
    });
  });

  describe('getMinimalNote', () => {
    it('on natural note, returns the same note', () => {
      ['A', 'B', 'C', 'D', 'E', 'F', 'G'].forEach((naturalNote) => {
        expect(noteService.getMinimalNote(naturalNote)).toBe(naturalNote);
      });
    });
    it('on sharp note, returns the same note', () => {
      ['A#', 'C#', 'D#', 'F#', 'G#'].forEach((naturalNote) => {
        expect(noteService.getMinimalNote(naturalNote)).toBe(naturalNote);
      });
    });
    it('on flat note, returns the sharp note', () => {
      expect(noteService.getMinimalNote('B♭')).toBe('A#');
    });
  });

  describe('getStepsAfter', () => {
    it('if not is not valid, will throw an error', () => {
      expect(() => {
        noteService.get5J('U');
      }).toThrow();
    });
    it('returns a note', () => {
      const quinte = noteService.getStepsAfter('A', 2);
      expect(noteService.isValid(quinte)).toBe(true);
    });

    it('returns a note', () => {
      const quinte = noteService.getStepsAfter('A', 2);
      expect(noteService.isValid(quinte)).toBe(true);
    });

    it('works if overlapping list size', () => {
      expect(noteService.getStepsAfter('G#', 1)).toBe('A');
    });

    it('works if not overlapping list size', () => {
      expect(noteService.getStepsAfter('G', 1)).toBe('G#');
    });
  });

  describe('curried methods', () => {
    let noteSpy;

    beforeEach(() => {
      noteSpy = jest.spyOn(noteService, 'getStepsAfter');
    });

    it('get2m calls getStepsAfter with the note and 1 steps', () => {
      noteService.get2m('A');
      expect(noteSpy).toHaveBeenCalledWith('A', 1);
    });

    it('get2M calls getStepsAfter with the note and 2 steps', () => {
      noteService.get2M('A');
      expect(noteSpy).toHaveBeenCalledWith('A', 2);
    });
    it('get3m calls getStepsAfter with the note and 3 steps', () => {
      noteService.get3m('A');
      expect(noteSpy).toHaveBeenCalledWith('A', 3);
    });
    it('get3M calls getStepsAfter with the note and 4 steps', () => {
      noteService.get3M('A');
      expect(noteSpy).toHaveBeenCalledWith('A', 4);
    });

    it('get4J calls getStepsAfter with the note and 5 steps', () => {
      noteService.get4J('A');
      expect(noteSpy).toHaveBeenCalledWith('A', 5);
    });
    it('get4aug calls getStepsAfter with the note and 6 steps', () => {
      noteService.get4aug('A');
      expect(noteSpy).toHaveBeenCalledWith('A', 6);
    });
    it('get5J calls getStepsAfter with the note and 7 steps', () => {
      noteService.get5J('A');
      expect(noteSpy).toHaveBeenCalledWith('A', 7);
    });
    it('get6m calls getStepsAfter with the note and 8 steps', () => {
      noteService.get6m('A');
      expect(noteSpy).toHaveBeenCalledWith('A', 8);
    });
    it('get6M calls getStepsAfter with the note and 9 steps', () => {
      noteService.get6M('A');
      expect(noteSpy).toHaveBeenCalledWith('A', 9);
    });
  });

  describe('Functional tests', () => {
    it('get2m', () => {
      expect(noteService.get2m('C')).toEqual('C#');
    });
    it('get2M', () => {
      expect(noteService.get2M('C')).toEqual('D');
    });
    it('get3m', () => {
      expect(noteService.get3m('C')).toEqual('D#');
    });
    it('get3M', () => {
      expect(noteService.get3M('C')).toEqual('E');
      expect(noteService.get3M('D')).toEqual('F#');
      expect(noteService.get3M('B')).toEqual('D#');
    });
    it('get4J', () => {
      expect(noteService.get4J('C')).toEqual('F');
    });
    it('get4aug', () => {
      expect(noteService.get4aug('C')).toEqual('F#');
    });
    it('get5J', () => {
      expect(noteService.get5J('C')).toEqual('G');
      expect(noteService.get5J('B')).toEqual('F#');
    });
    it('get6m', () => {
      expect(noteService.get6m('C')).toEqual('G#');
    });
    it('get6M', () => {
      expect(noteService.get6M('C')).toEqual('A');
    });
  });

  describe('getStepsAfterWithHeight', () => {
    let defaultA: NoteWithHeight;
    let defaultC: NoteWithHeight;

    beforeEach(() => {
      defaultA = {
        note: 'A',
        height: 4,
      };
      defaultC = {
        note: 'C',
        height: 0,
      };
    });

    it('returns identity on note if provided with 0 steps', () => {
      expect(noteService.getStepsAfterWithHeight(defaultA, 0).note).toBe('A');
    });

    it('calls getStepsAfter in order to get the note', () => {
      const getStepsAfterSpy = jest.spyOn(noteService, 'getStepsAfter');
      noteService.getStepsAfterWithHeight(defaultA, 0);
      expect(getStepsAfterSpy).toHaveBeenCalledWith(defaultA.note, 0);
    });

    it('12 steps after C0 is C1', () => {
      expect(noteService.getStepsAfterWithHeight(defaultC, 12).height).toBe(1);
    });

    it('12 steps after C0 is C1', () => {
      expect(noteService.getStepsAfterWithHeight(defaultC, 12).height).toBe(1);
    });

    it('2 steps after B0 has 1 as height', () => {
      expect(
        noteService.getStepsAfterWithHeight({ note: 'B', height: 0 }, 2).height
      ).toBe(1);
    });
  });

  describe('getDistanceFromScaleStart', () => {
    it('returns 0 if C', () => {
      expect(noteService.getDistanceFromScaleStart('C')).toBe(0);
    });
    it('returns 1 if C#', () => {
      expect(noteService.getDistanceFromScaleStart('C#')).toBe(1);
    });
    it('returns 11 if B', () => {
      expect(noteService.getDistanceFromScaleStart('B')).toBe(11);
    });
  });
});
