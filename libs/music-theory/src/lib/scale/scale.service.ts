import { List, Map, Set } from 'immutable';
import { ChordService } from '../chord/chord.service';
import { MinimalDictArray } from '../note/anglican_notes_minimal';
import { NoteService } from '../note/note.service';


export interface ValuedItem {
  note: string;
  value: number;
}

export interface ValuedMode {
  key: string;
  mode: string;
  similarity: number;
  result: Array<string>;
}

export class ScaleService {
  majorPentatonicScale = (note) => [
    this.noteService.get1J(note),
    this.noteService.get2M(note),
    this.noteService.get3M(note),
    this.noteService.get5J(note),
    this.noteService.get6M(note),
  ];

  minorPentatonicScale = (note) => [
    this.noteService.get1J(note),
    this.noteService.get3m(note),
    this.noteService.get4J(note),
    this.noteService.get5J(note),
    this.noteService.get7m(note),
  ];

  majorScale = (note) => [
    this.noteService.get1J(note),
    this.noteService.get2M(note),
    this.noteService.get3M(note),
    this.noteService.get4J(note),
    this.noteService.get5J(note),
    this.noteService.get6M(note),
    this.noteService.get7M(note),
  ];

  minorScale = (note) => [
    this.noteService.get1J(note),
    this.noteService.get2M(note),
    this.noteService.get3m(note),
    this.noteService.get4J(note),
    this.noteService.get5J(note),
    this.noteService.get6m(note),
    this.noteService.get7m(note),
  ];

  ionianMode = (note) => this.majorScale(note);
  aeolianMode = (note) => this.minorScale(note);
  locrianMode = (note) => [
    this.noteService.get1J(note),
    this.noteService.get2m(note),
    this.noteService.get3m(note),
    this.noteService.get4J(note),
    this.noteService.get5dim(note),
    this.noteService.get6m(note),
    this.noteService.get7m(note),
  ];
  dorianMode = (note) => [
    this.noteService.get1J(note),
    this.noteService.get2M(note),
    this.noteService.get3m(note),
    this.noteService.get4J(note),
    this.noteService.get5J(note),
    this.noteService.get6M(note),
    this.noteService.get7m(note),
  ];
  phrygianMode = (note) => [
    this.noteService.get1J(note),
    this.noteService.get2m(note),
    this.noteService.get3m(note),
    this.noteService.get4J(note),
    this.noteService.get5J(note),
    this.noteService.get6m(note),
    this.noteService.get7m(note),
  ];
  lydianMode = (note) => [
    this.noteService.get1J(note),
    this.noteService.get2M(note),
    this.noteService.get3M(note),
    this.noteService.get4aug(note),
    this.noteService.get5J(note),
    this.noteService.get6M(note),
    this.noteService.get7M(note),
  ];
  mixolydianMode = (note) => [
    this.noteService.get1J(note),
    this.noteService.get2M(note),
    this.noteService.get3M(note),
    this.noteService.get4J(note),
    this.noteService.get5J(note),
    this.noteService.get6M(note),
    this.noteService.get7m(note),
  ];

  constructor(
    private noteService: NoteService,
    private chordService: ChordService
  ) {}

  generateScale(note: string) {
    if (note.endsWith('m')) {
      const chordStrLength = note.length;
      return this.minorPentatonicScale(note.substring(0, chordStrLength - 1));
    } else {
      return this.majorPentatonicScale(note);
    }
  }

  getSimilaritiesWithModeOnKey(
    valuedNotes: List<ValuedItem>,
    mode: (string) => Array<string>,
    key: string
  ) {
    const _modeSet = Set(mode(key));
    let score = 0;
    valuedNotes.forEach((valuedItem: ValuedItem) => {
      if (_modeSet.has(valuedItem.note)) {
        score += valuedItem.value;
      }
    });
    return score;
  }

  getMostProbableMode(chordProgression: List<string>): ValuedMode {
    let notesMap = Map<string, number>();
    const chordNotes = chordProgression.map((chord: string) =>
      this.chordService.generateChord(chord)
    );
    chordNotes.forEach((chordComposition: Array<string>) => {
      chordComposition.forEach((note: string) => {
        const oldNoteOccurence = notesMap.get(note);
        const newNoteOccurence =
          oldNoteOccurence !== null && oldNoteOccurence !== undefined
            ? oldNoteOccurence + 1
            : 1;
        notesMap = notesMap.set(note, newNoteOccurence);
      });
    });
    const valuedList: List<ValuedItem> = List(
      Array.from(notesMap).map((noteMapItem: [string, number]) => ({
        note: noteMapItem[0],
        value: noteMapItem[1],
      }))
    );

    const modeList = List([
      {
        name: 'Aeolian',
        modeFn: this.aeolianMode,
      },
      {
        name: 'Dorian',
        modeFn: this.dorianMode,
      },
      {
        name: 'Ionian',
        modeFn: this.ionianMode,
      },
      {
        name: 'Locrian',
        modeFn: this.locrianMode,
      },
      {
        name: 'Lydian',
        modeFn: this.lydianMode,
      },
      {
        name: 'Mixolydian',
        modeFn: this.mixolydianMode,
      },
      {
        name: 'Phrygian',
        modeFn: this.phrygianMode,
      },
    ]);

    let results = List([]);

    MinimalDictArray.forEach((note) => {
      modeList.forEach((modeFn) => {
        results = results.push({
          key: note,
          mode: modeFn.name,
          similarity: this.getSimilaritiesWithModeOnKey(
            valuedList,
            modeFn.modeFn,
            note
          ),
          result: modeFn.modeFn(note),
        });
      });
    });

    const isInArray = (elem, arr) => !!(arr.indexOf(elem) + 1); // TODO move it to utils

    results = results.sort((valueA, valueB) => {
      if (valueA.similarity >= valueB.similarity) {
        if (valueA.similarity === valueB.similarity) {
          if (
            isInArray(valueA.key, chordProgression) &&
            !isInArray(valueB.key, chordProgression)
          ) {
            return -1;
          }
        }
        return -1;
      } else {
        return 1;
      }
    });
    return results.get(0);
  }
}
