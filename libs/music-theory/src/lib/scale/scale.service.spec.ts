import { ChordService } from '../chord/chord.service';
import { NoteService } from '../note/note.service';
import { ScaleService } from './scale.service';

describe('ScaleService', () => {
  let noteService: NoteService;
  let scaleService: ScaleService;
  let chordService: ChordService;

  beforeEach(() => {
    noteService = new NoteService();
    chordService = new ChordService(noteService);

    scaleService = new ScaleService(
      noteService,
      chordService
    );
  });

  describe('scales', () => {
    it('e2e : C major pentatonique scale', () => {
      expect(scaleService.majorPentatonicScale('C')).toEqual([
        'C',
        'D',
        'E',
        'G',
        'A',
      ]);
    });
    it('e2e : E minor pentatonique scale', () => {
      expect(scaleService.minorPentatonicScale('E')).toEqual([
        'E',
        'G',
        'A',
        'B',
        'D',
      ]);
    });
    it('e2e : C major scale', () => {
      expect(scaleService.majorScale('C')).toEqual([
        'C',
        'D',
        'E',
        'F',
        'G',
        'A',
        'B',
      ]);
    });
    it('e2e : C minor scale', () => {
      expect(scaleService.minorScale('C')).toEqual([
        'C',
        'D',
        'D#',
        'F',
        'G',
        'G#',
        'A#',
      ]);
    });

    it('e2e : C Aeolian', () => {
      expect(scaleService.aeolianMode('C')).toEqual([
        'C',
        'D',
        'D#',
        'F',
        'G',
        'G#',
        'A#',
      ]);
    });

    it('e2e : C Dorian', () => {
      expect(scaleService.dorianMode('C')).toEqual([
        'C',
        'D',
        'D#',
        'F',
        'G',
        'A',
        'A#',
      ]);
    });

    it('e2e : C Locrian', () => {
      expect(scaleService.locrianMode('C')).toEqual([
        'C',
        'C#',
        'D#',
        'F',
        'F#',
        'G#',
        'A#',
      ]);
    });

    it('e2e : C Phrigian', () => {
      expect(scaleService.phrygianMode('C')).toEqual([
        'C',
        'C#',
        'D#',
        'F',
        'G',
        'G#',
        'A#',
      ]);
    });

    it('e2e : C Lydian', () => {
      expect(scaleService.lydianMode('C')).toEqual([
        'C',
        'D',
        'E',
        'F#',
        'G',
        'A',
        'B',
      ]);
    });

    it('e2e : C Mixolydian', () => {
      expect(scaleService.mixolydianMode('C')).toEqual([
        'C',
        'D',
        'E',
        'F',
        'G',
        'A',
        'A#',
      ]);
    });
  });
});
