export enum Modes {
  IONIAN = 1,
  DORIAN = 2,
  PHRYGIAN = 3,
  LYDIAN = 4,
  MIXOLYDIAN = 5,
  AEOLIAN = 6,
  LOCRIAN = 7,
}
