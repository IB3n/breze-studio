# Breze.studio

this project is a frontend only web application initially written in 2018.

## Scope

Breze.studio is a music creation assistant.

## Roadmap

It's funny to get a hold on an abondonned project written by oneself some years ago, broken tests ugly things etc... A perfect occasion of improving a codebase.

- [x] migrate to an nx workspace (J+0)
- [x] dockerize build (J+1)
- [x] publish to gitlab pages
- [ ] repair tests
- [ ] extract core rules into a lib in the workspace
- [ ] Redesign this old thing, make it mobile first
